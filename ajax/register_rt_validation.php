<?php 
require("../system/functions.php");
$message = array();
$_POST['step'] = 'step1';

$result = array(
	'status' => FALSE,
	'message' => $message,
);

if(count($_POST) > 0){
	$step_validation = array(
		'step1' => array(
			'name' => array(
				'label' => "Nama", 
				'type' => "text",
				'maxlength' => 50,
				'required' => true
			),
			'phone' => array(
				'label' => "No. HP", 
				'type' => "number",
				'maxlength' => 15,
				'required' => true
			),
			'rt' => array(
				'label' => "No. RT", 
				'type' => "number",
				'maxlength' => 11,
				'required' => true
			),
			'rw' => array(
				'label' => "No. RW", 
				'type' => "number",
				'maxlength' => 11,
				'required' => true
			),
			'province' => array(
				'label' => "Provinsi", 
				'maxlength' => 11,
				'required' => true
			),
			'city' =>array(
				'label' => "Kabupaten / Kota", 
				'maxlength' => 11,
			), 
			'city_tmp' =>array(
				'label' => "Kabupaten / Kota Tambahan", 
				'maxlength' => 50,
			), 
			'district' => array(
				'label' => "Kecamatan", 
				'maxlength' => 11,
			),
			'district_tmp' => array(
				'label' => "Kecamatan Tambahan", 
				'maxlength' => 50,
			),
			'village' =>  array(
				'label' => "Desa", 
				'type' => "text",
				'maxlength' => 50,
				'required' => true
			),
			'country' =>  array(
				'label' => "Negara", 
				'type' => "text",
				'maxlength' => 50,
				'required' => true
			),
		)
	);
	$current_step = $step_validation[$_POST['step']];

	unset($_POST['step']);
	$result['status'] = TRUE;

	foreach($current_step as $_key => $_value){
		$type = ( isset($_value['type']) ) ? $_value['type'] : null;
		$maxlength = ( isset($_value['maxlength']) ) ? intval($_value['maxlength']) : null;
		if(@$_value['required'] == true){
			if (@$_POST[$_key] == "" || !isset($_POST[$_key])) {
				$result['status'] = FALSE;
				$message[$_key][] = $_value['label'] . ' tidak boleh kosong';
			}
		}
		if(@$type == "text"){
			if(!preg_match('/[a-zA-Z\s]+/',@$_POST[$_key])) {
				$result['status'] = FALSE;
				$message[$_key][] = $_value['label'] . " harus berupa alfabet";
			}
		}
		if(@$type == "email"){
			if (!filter_var(@$_POST[$_key], FILTER_VALIDATE_EMAIL)) {
				$result['status'] = FALSE;
				$message[$_key][] = $_value['label'] . " email tidak valid";
			}
		}
		if(@$type == "number"){
			if (!is_numeric(@$_POST[$_key])) {
				$result['status'] = FALSE;
				$message[$_key][] = $_value['label'] . ' harus berupa angka';
			}
		}
		// Checking Maxlength
		if(@$maxlength != null){
			if (strlen(@$_POST[$_key]) > $maxlength) {
				$result['status'] = FALSE;
				$message[$_key][] = $_value['label'] . ' hanya boleh berisi ' . $maxlength . ' karakter';
			}
		}
	}

	$result['message'] = $message;
	echo json_encode($result);
} else {
	echo json_encode($result);
}
?>