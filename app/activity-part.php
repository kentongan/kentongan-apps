<?php
require_once '../system/functions.php';

if(isset($_GET['page'])) {
  $page = $_GET['page'];
} else {
  $page = 1;
}
$activities = $functions->getActivity($page);

if ($activities['data']['count'] <= 0 || count($activities['data']['rows']) <= 0) {
  echo '<p>Belum ada aktivitas terbaru, silakan mulai menambahkan warga, mengisi kegiatan atau laporan kas</p>';
} else {

foreach ($activities['data']['rows'] as $activity) {
  $content = json_decode($activity['content'], true);
  ?>
  <li class="media">
    <div class="media-left">
      <a href="#">
        <?php
        $icon = $baseurl . '/assets/images/icon/';
        switch($activity['type']) {
          case 'status':
            $icon .= 'icon-ballon.png';
            break;
          case 'people':
            $icon .= 'icon-group.png';
            break;
          case 'finance':
            $icon .= 'icon-usd.png';
            break;
          case 'alert':
            $icon .= 'icon-danger.png';
            break;
          case 'event':
            $icon .= 'icon-info.png';
            break;
          default:
            $icon .= 'icon-ballon.png';
            break;
        }
        ?>
        <img class="media-object" src="<?php echo $icon?>" alt="Kentongan">
      </a>
    </div>
    <div class="media-body">
      <h4 class="media-heading">
        <?php if ($activity['type'] == 'status') { ?>
        <a href="<?php echo $baseurl . '/app/detail.php?activity_id=' . $activity['id']  ?>"><?php echo $activity['user']['name'] . " " . $activity['title'] ?></a>
        <?php } else { ?>
        <a href="<?php echo $baseurl . '/app/detail.php?activity_id=' . $activity['id']  ?>"><?php echo $activity['title']; ?></a>
        <?php } ?>
      </h4>
      <?php
      if ($activity['type'] == 'status') { ?>
        <p><?php echo $content['text']; ?></p>
      <?php } elseif($activity['type'] == 'finance') {
        // $content = json_decode($activity['content'], true);
      ?>
      <table>
        <tr>
          <td>Keterangan</td>
          <td style="width:6px;">:</td>
          <td><?php echo $content['description'] ?></td>
        </tr>
        <tr>
          <td>Nominal</td>
          <td style="width:6px;">:</td>
          <td>Rp <?php echo number_format($content['amount'],0,",",".") ?></td>
        </tr>
        <tr>
          <td>Jenis</td>
          <td style="width:6px;">:</td>
          <td><?php echo $content['tag'] ?></td>
        </tr>
      </table>
      <?php }
      elseif ($activity['type'] == 'event') {
        // $content = json_decode($activity['content'], true);
        $dateFormatter = new IntlDateFormatter('id_ID.UTF-8', IntlDateFormatter::LONG, IntlDateFormatter::LONG);
        $dateFormatter->setPattern('EEEE, d MMMM y'); // Selasa, 19 Agustus 2016

        $start = (new DateTime())->setTimestamp($content['start_date']);
        $end = (new DateTime())->setTimestamp($content['end_date']);
      ?>
      <table class="activity">
        <tr>
          <td>Mulai</td>
          <td style="width:6px;">:</td>
          <td><?php echo $dateFormatter->format($start) . ' - ' . $start->format('H:i'); ?></td>
        </tr>
        <tr>
          <td>Selesai</td>
          <td style="width:6px;">:</td>
          <td><?php echo $dateFormatter->format($end) . ' - ' . $end->format('H:i'); ?></td>
        </tr>
        <tr>
          <td>Lokasi</td>
          <td style="width:6px;">:</td>
          <td><?php echo $content['location'] ?></td>
        </tr>
        <tr>
          <td>Deskripsi</td>
          <td style="width:6px;">:</td>
          <td><?php echo $content['description'] ?></td>
        </tr>
      </table>
      <?php }
      elseif ($activity['type'] == 'alert') {
        // $content = json_decode($activity['content'], true);
        if(!$content){
          echo "<div class='activity'>Lokasi tidak tersedia</div>";
        }
        else {
      ?>
          <div id="address_<?php echo $activity['id']; ?>" data-lat="<?php echo $content['latitude']; ?>" data-lng="<?php echo $content['longitude']; ?>"></div>
          <div style="height: 300px" class="map unloaded" id="map_<?php echo $activity['id']; ?>" data-lat="<?php echo $content['latitude']; ?>" data-lng="<?php echo $content['longitude']; ?>"></div>
      <?php
        }
      }
      elseif ($activity['type'] == 'people') {
        // $content = json_decode($activity['content'], true);
      ?>
      <table class="activity">
        <tr>
          <td>Nama</td>
          <td style="width:6px;">:</td>
          <td><?php echo $content['name']; ?></td>
        </tr>
        <?php if (isset($content['address'])) { ?>
        <tr>
          <td>Alamat</td>
          <td style="width:6px;">:</td>
          <td><?php echo $content['address']; ?></td>
        </tr>
        <?php } ?>
      </table>
      <?php
      } elseif ($activity['type'] == 'report') { ?>
      <p><?php echo $content['description']; ?></p>
      <?php } else { ?>
      <p><?php echo $activity['content'] ?></p>
      <?php } ?>

      <span><?php echo $functions->timeAgo($activity['created']) ?></span>
      <span class="pull-right"><a href="<?php echo $baseurl . '/app/detail.php?activity_id=' . $activity['id']  ?>"><?php echo $activity['comment_count'] ?> Komentar</a></span>
    </div>
  </li>
<?php
}
}
if ($activities['data']['total'] > $page) {
?>
<a class="next" href="<?php echo $baseurl . '/app/activity-part.php?page=' . ($page + 1) ?>">Next</a>
<?php } ?>
