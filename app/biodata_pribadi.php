<?php require("../header.php"); ?>
<div id="page-wrapper">
    <div class="container-fluid">
        <div id="main-content">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="biodata_pribadi.php">Biodata Pribadi</a></li>
                <li role="presentation"><a href="keluarga.php">Keluarga</a></li>
            </ul>
            <div class="card-content">
                <?php
                $data = $functions->getUser($_COOKIE['peopleid']);
                $data = $data[0];
                if( count($_POST) > 0){
                    unset($_POST['date_']);
                    $edit = $functions->updateUser($_COOKIE['peopleid'], $_POST);
                    if ($edit->status) {
                        $data = $edit->data->rows[0];
                        echo "<div class='alert alert-success'>Biodata berhasil diubah</div>";
                        //header("location:biodata_pribadi.php?status=1");
                    } else {
                        $data = $edit->data->rows;
                        foreach ($_POST as $key => $value) {
                            $data->{$key} = $value;        
                        }
                        $messages = array();
                        if ($edit->message == 'Validation Error') {
                            foreach ($edit->data->rows as $key => $value) {
                                foreach ($value as $message) {
                                    $messages[] = strtolower($message);
                                }
                            }
                        } else {
                            $messages[] = strtolower($edit->message);
                        }
                        echo "<div class='alert alert-danger'>Biodata tidak berhasil diubah: ". implode(', ', $messages). " </div>";
                    }

                    //echo '<script type="text/javascript">$("#main-content").load( "warga.php'.$tokenurl.'" );</script>';
                }
                //$data = $kentongan->select("peoples",array('peopleid' => $_COOKIE['peopleid']));
                //$birth_date = date('Y-m-d', $data->birth_date);
                $max_year = date('Y');
                ?>
              
                <div class="row">
                    <div class="col-lg-12">
                       <!--  <h1 class="page-header">
                            <small>Sesuaikan Data Pribadi</small>
                            <a class="btn btn-primary pull-right btn-plus" title="Keluarga" href="keluarga.php">
                                <img height="24" src="/assets/images/icon/icon-keluarga.png" alt="kentongan">
                            </a>
                        </h1> -->
                        <!-- Nav tabs -->
                    </div>
                </div>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="biodata">
                        <form class="form" method="POST" enctype="multipart/form-data" action="biodata_pribadi.php">
                            <div class="form-group">
                                <label class="form-label">Nama</label>
                                <div class="input-group col-md-12">
                                    <input value="<?php echo $data->name; ?>" type="text" name="name" id="name" class="form-control" placeholder="Nama" maxlength="75" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Alamat</label>
                                <div class="input-group col-md-12">
                                    <textarea name="address" id="address" class="form-control" placeholder="Alamat"><?php echo $data->address; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Tanggal Lahir</label>
                                <div class="input-group col-md-12 date">
                                    <input id="birth_date" name="birth_date" class="form-control date" data-format="dd/MM/yyyy hh:mm:ss" placeholder="Tanggal Lahir" type="date" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Tempat Lahir</label>
                                <div class="input-group col-md-12">
                                    <input value="<?php echo $data->birth_place; ?>" type="text" name="birth_place" id="birthplace" class="form-control" placeholder="Tempat Lahir" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Jenis Kelamin</label>
                                <div class="input-group col-md-12">
                                    <span class="input-group-addon"><i class="fa fa-mars"></i></span>
                                    <label for="gender" class="select-box">
                                        <select name="gender" class="form-control" id="gender">
                                            <option value="L" <?php echo ($data->gender == "L") ? "selected" : null; ?>>Laki Laki</option>
                                            <option value="P" <?php echo ($data->gender == "P") ? "selected" : null; ?>>Perempuan</option>
                                        </select>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">No. HP</label>
                                <div class="input-group col-md-12">
                                    <input value="<?php echo $data->phone; ?>" type="text" name="phone" id="mobile" class="form-control" placeholder="Nomor HP" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Email</label>
                                <div class="input-group col-md-12">
                                    <input value="<?php echo $data->email; ?>" type="text" name="email" id="email" class="form-control" placeholder="Email" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Tingkat Pendidikan</label>
                                <div class="input-group col-md-12">
                                    <select name="education" class="form-control" id="educationlevel">
                                        <option value="Tidak Sekolah" <?php echo ($data->education == 'Tidak Sekolah') ? 'selected' : ''; ?>>Tidak Sekolah</option>
                                        <option value="TK" <?php echo ($data->education == 'TK') ? 'selected' : ''; ?>>TK</option>
                                        <option value="SD" <?php echo ($data->education == 'SD') ? 'selected' : ''; ?>>SD</option>
                                        <option value="SMP" <?php echo ($data->education == 'SMP') ? 'selected' : ''; ?>>SMP</option>
                                        <option value="SMA/SMK" <?php echo ($data->education == 'SMA/SMK') ? 'selected' : ''; ?>>SMA/SMK</option>
                                        <option value="D1" <?php echo ($data->education == 'D1') ? 'selected' : ''; ?>>D1</option>
                                        <option value="D2" <?php echo ($data->education == 'D2') ? 'selected' : ''; ?>>D2</option>
                                        <option value="D3" <?php echo ($data->education == 'D3') ? 'selected' : ''; ?>>D3</option>
                                        <option value="S1" <?php echo ($data->education == 'S1') ? 'selected' : ''; ?>>S1</option>
                                        <option value="S2" <?php echo ($data->education == 'S2') ? 'selected' : ''; ?>>S2</option>
                                        <option value="S3" <?php echo ($data->education == 'S3') ? 'selected' : ''; ?>>S3</option>
                                    </select>
                                    <!--<input value="<?php /*echo $data->education; */?>" type="text" name="education" id="educationlevel" class="form-control" placeholder="Tingkat Pendidikan" />-->
                                </div>
                            </div>
                            <?php if($_COOKIE['kentongantype'] == "rt" || $_COOKIE['kentongantype'] == "1")  { ?>
                            <div class="form-group">
                                <label class="form-label">Status Warga / RT</label>
                                <div class="input-group col-md-12">
                                    <select name="role" id="peoplestatus" class="form-control">
                                        <option value="rt" <?php echo ($data->role == 'rt') ? "selected" : null ?>>RT</option>
                                        <option value="wr" <?php echo ($data->role == 'wr') ? "selected" : null ?>>Warga</option>
                                    </select>
                                </div>
                            </div>
                            <?php } ?>
                            <?php /*
                            <div class="form-group">
                                <label class="form-label">Nomor KTP</label>
                                <div class="input-group col-md-12">
                                    <input value="<?php echo $data->card_id; ?>" type="text" name="card_id" id="card_id" class="form-control" placeholder="No. KTP / NIK (Optional)" />
                                </div>
                            </div>
                            */ ?>
                            <div class="form-group">
                                <label class="form-label">Kode Keluarga</label>
                                <div class="input-group col-md-12">
                                    <input value="<?php echo $data->family_id; ?>" type="text" name="family_id" id="family_id" class="form-control" placeholder="Kode yang sama adalah satu keluarga" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Foto</label>
                                <div class="input-group col-md-12">
                                    <label for="photo_img">
                                        <input type="file" name="photo_img" id="photo_img" class="form-control" placeholder="Foto"  onclick="choosePhoto();" />
                                    </label>
                                    <input type="hidden" name="photo" id="photo" value="" />
                                    <div id="files" class="files">
                                        <img class="photo_img_preview" id="photo_preview" src="<?php echo $data->photo; ?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group col-md-12">
                                    <button type="submit" class="btn-primary btn">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- <div role="tabpanel" class="tab-pane fade " id="keluarga">
                        <?php //require("keluarga.php"); ?>
                    </div> -->
                </div>
            </div>
            <div class="form-group">
                <div class="input-group col-md-12">
                    <a href="/logout.php">
                        <button type="button" class="btn-danger btn">Log Out</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
<script type="text/javascript">
    var maxYear = "<?php date('Y'); ?>";
    var defaultDate = "<?php echo @$data->birth_date; ?>";
</script>
<script type="text/javascript">
    function showAndroidToast(toast) {
        Android.showToast(toast);
    }
    function setFilePath(file) {
        console.log( file );
        document.getElementById('photo').value = file;
        Android.showToast(file);
    }
    function setFileUri(uri) {
        document.getElementById('photo_preview').src = uri;
        Android.showToast(uri);
    }
    function choosePhoto() {
        var file = Android.choosePhoto();
    }
</script>
<?php require("../footer.php"); ?>
