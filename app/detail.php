  <?php
  require("../header.php");

  if (!isset($_GET['activity_id'])){
    redirect($baseurl . "/app/detail_rt.php");
    die();
  }
  $activityId = $_GET['activity_id'];
  // POST request, User post comment
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $content = $_POST['content'];
    if (!$content) {
      echo '<div class="alert alert-danger">Komentar tidak boleh kosong</div>';
    } else {

      $result = $functions->postComment($activityId, $content);

      //If error happens
      if ($result['code'] != 200) {
        $messages = [];
        if($result['message'] = 'Validation Error') {
          foreach ($result['data']['rows'] as $key => $value) {
            foreach ($value as $message) {
              $messages[] = strtolower($message);
            }
          }
        } else {
          $messages[] = strtolower($sendalarm->message);
        }
        echo "<div class='alert alert-danger'>". implode(', ', $messages) . "</div>";
      }
    }

  }

  $result = $functions->getSingleActivity($activityId);

  // Not found
  if (!$result || $result['code'] != 200 || $result['data']['total'] <= 0){
    header('Location: ' . $baseurl);
    die();
  }

  $activity = $result['data']['rows'][0];
  $activityContent = json_decode($activity['content'], true);

  $result = $functions->getComments($activityId);
  if (!$comments = $result['data']['rows']){
    $comments = [];
  }

  $loggedUser = $functions->getUserLogged()[0];

  ?>
<div id="page-wrapper">
  <div class="container-fluid">
    <div id="main-content">
      <div class="card-content">
        <div class="row">
          <div class="col-lg-12">
            <h1 class="page-header">
              <small>Komentar</small>
              <a class="btn btn-primary pull-right btn-back" href="<?php echo $baseurl . "/app/detail_rt.php"; ?>"><i class="ion-android-arrow-back"></i></a>
            </h1>
          </div>
        </div>
        <div class="wrapper">
          <div class="main-content">
            <section>
              <ul class="media-list">
                <li class="media">
                  <div class="media-left">
                    <a href="#">

                      <?php
                      $icon = $baseurl . '/assets/images/icon/';
                      switch($activity['type']) {
                        case 'status':
                          $icon .= 'icon-ballon.png';
                          break;
                        case 'people':
                          $icon .= 'icon-group.png';
                          break;
                        case 'finance':
                          $icon .= 'icon-usd.png';
                          break;
                        case 'alert':
                          $icon .= 'icon-danger.png';
                          break;
                        case 'event':
                          $icon .= 'icon-info.png';
                          break;
                        default:
                          $icon .= 'icon-ballon.png';
                          break;
                      }
                      ?>
                      <img class="media-object" src="<?php echo $icon; ?>" alt="Kentongan">
                    </a>
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading">
                      <?php if ($activity['type'] == 'status') { ?>
                      <?php echo $activity['user']['name'] . " " . $activity['title'] ?>
                      <?php } else {
                        echo $activity['title'];
                      } ?>
                    </h4>
                    <?php
                    if ($activity['type'] == 'status') { ?>
                      <p><?php echo $activityContent['text']; ?></p>
                    <?php
                    } elseif($activity['type'] == 'finance') {
                      // $content = json_decode($activity['content']);
                      ?>
                      <table>
                        <tr>
                          <td>Keterangan</td>
                          <td>: <?php echo $activityContent['description'] ?></td>
                        </tr>
                        <tr>
                          <td>Nominal</td>
                          <td>: Rp <?php echo number_format($activityContent['amount'],0,",",".") ?></td>
                        </tr>
                        <tr>
                          <td>Jenis</td>
                          <td>: <?php echo $activityContent['tag'] ?></td>
                        </tr>
                      </table>
                      <?php }
                      elseif ($activity['type'] == 'event') {
                        // $content = json_decode($activity['content']);
                        $dateFormatter = new IntlDateFormatter('id_ID.UTF-8', IntlDateFormatter::LONG, IntlDateFormatter::LONG);
                        $dateFormatter->setPattern('EEEE, d MMMM y'); // Selasa, 19 Agustus 2016

                        $start = (new DateTime())->setTimestamp($activityContent['start_date']);
                        $end = (new DateTime())->setTimestamp($activityContent['end_date']);
                        ?>
                        <table>
                          <tr>
                            <td style="width:80px;">Mulai</td>
                            <td>:</td>
                            <td><?php echo $dateFormatter->format($start) . ' - ' . $start->format('H:i'); ?></td>
                          </tr>
                          <tr>
                            <td style="width:80px;">Selesai</td>
                            <td>:</td>
                            <td><?php echo $dateFormatter->format($end) . ' - ' . $end->format('H:i'); ?></td>
                          </tr>
                          <tr>
                            <td style="width:80px;">Lokasi</td>
                            <td>:</td>
                            <td><?php echo $activityContent['location'] ?></td>
                          </tr>
                          <tr>
                            <td style="width:80px;">Deskripsi</td>
                            <td>:</td>
                            <td><?php echo $activityContent['description'] ?></td>
                          </tr>
                        </table>
                        <?php }
                        elseif ($activity['type'] == 'alert') {
                          // $content = json_decode($activity['content']);
                          if (!$activityContent){
                            echo '<p>Lokasi Tidak Tersedia</p>';
                          } else {
                            ?>
                            <div style="height: 300px" id="map"></div>
                            <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwydKMEo7-u0CU7PRoAPi9lA71uDtiqDU&signed_in=false&callback=initMap"></script>
                            <script type="text/javascript">
                // Google map init function
                initMap = function(){
                  var pos = {
                    lat: <?php echo $activityContent['latitude'] ?>,
                    lng: <?php echo $activityContent['longitude'] ?>
                  };

                  var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 18,
                    center: pos
                  });

                  new google.maps.Marker({
                    position: pos,
                    map: map,
                    title: 'Lokasi Pengirim'
                  });

                }
                </script>
                <?php
                  }
                } elseif ($activity['type'] == 'people') {
                  // $content = json_decode($activity['content'], true);
                ?>
                <table class="activity">
                  <tr>
                    <td>Nama</td>
                    <td style="width:6px;">:</td>
                    <td><?php echo $activityContent['name']; ?></td>
                  </tr>
                  <?php if (isset($activityContent['address'])) { ?>
                  <tr>
                    <td>Alamat</td>
                    <td style="width:6px;">:</td>
                    <td><?php echo $activityContent['address']; ?></td>
                  </tr>
                  <?php } ?>
                </table>
                <?php
                } elseif ($activity['type'] == 'report') { ?>
                <p><?php echo $activityContent['description']; ?></p>
                <?php } else { ?>
                <p><?php echo $activity['content'] ?></p>
                <?php } ?>

                <span><?php echo $functions->timeAgo($activity['created']) ?></span>
                </div>
                </li>
              </ul>
              <ul class="media-list comment-items">
                <?php
                $showIndexLimit = count($comments) - 4;
                if ($showIndexLimit > 0){
                echo '<li class="comments-scroll"><a id="show-comments" class="show-comments" href="#">Tampilkan '. $showIndexLimit .' komentar sebelumnya</a></li>';
                }
                foreach ($comments as $i => $comment) {
                ?>
                <li class="media comment-item <?php echo $i < $showIndexLimit?'hidden-comment':''; ?>">
                <div class="media-left">
                <a href="#">
                  <?php
                  $avatar = 'http://i.imgur.com/kfyG0il.png';
                  if($loggedUser->photo) {
                    $avatar = $loggedUser->photo;
                  }
                  ?>
                  <div class="img-container activity"><img class="media-object" data-original="<?php echo $avatar ?>" src="<?php echo $avatar ?>" alt="User Avatar"></div>
                </a>
                </div>
                <div class="media-body">
                <h4 class="media-heading"><a href="#"><?php echo $comment['user']['name'] ?></a></h4>
                <p><?php echo $comment['content'] ?></p>
                <span><?php echo $functions->timeAgo($comment['created']) ?></span>
                </div>
                </li>
                <?php
                }
                ?>
                <li class="media comment-item">
                <div class="media-left">
                <a href="#">
                <?php
                $avatar = 'http://i.imgur.com/kfyG0il.png';
                if($loggedUser->photo) {
                  $avatar = $loggedUser->photo;
                }
                ?>
                <div class="img-container activity"><img class="media-object" data-original="<?php echo $avatar ?>" src="<?php echo $avatar ?>" alt="User Avatar"></div>
                </a>
                </div>
                <div class="media-body">
                <h4 class="media-heading"><a href="#"><?php echo $loggedUser->name ?></a></h4>
                <form method="post" class="add-comment">
                <div class="form-group">
                  <textarea class="form-control" name="content" id="textComment" rows="3" placeholder="Tulis komentar..."></textarea>
                </div>
                <button type="submit" class="btn btn-primary pull-right">Kirim</button>
                </form>
                </div>
                </li>
              </ul>
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
require('../footer.php');
?>
