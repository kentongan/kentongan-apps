<?php require("../header.php"); ?>
<div id="page-wrapper">
  <div class="container-fluid">
    <div id="main-content">
      <div class="card-content">
        <?php
          if( @$_GET['page'] == "edit" ){
            if($_COOKIE['kentongantype']!="rt"){
              // echo "<script type='text/javascript'>window.location.href='detail_rt.php';</script>";
              redirect("detail_rt.php");
              exit;
            }
            // get Data RT
            $data = $functions->getRT($_COOKIE['rtid'])->data->rows[0];
            if(!isset($data)){
              echo "Data Not Found";
              exit;
            }
            if( count($_POST) > 0){
              $error = false;
              $empty=array();
              $fields = array(
                'rt' => "RT",
                'rw' => "RW",
                'province_id' => "Provinsi",
                'city_id' => "Kab / Kota",
                'district_id' => "Kecamatan",
                'village' => "Desa / Kelurahan",
                'approval_photo' => "Stempel RT",
              );
              foreach($_POST as $_key => $_value){
                if ($_key == 'approval_photo' && !$_value) {
                  $_value = $data->approval_photo;
                }
                if($_value == ""){
                  $error = true;
                  $empty[] = $fields[$_key];
                }
              }
              if(!$error){
                $edit = $functions->updateRT($_POST,$_COOKIE['rtid']);
                // echo '<pre>';print_r($edit);echo "</pre>";
                if ($edit->status == true) {
                  header("location:detail_rt.php");
                } else {
                  $messages = array();
                  if ($edit->message == 'Validation Error') {
                      foreach ($edit->data->rows as $key => $value) {
                          foreach ($value as $message) {
                              $messages[] = strtolower($message);
                          }
                      }
                  } else {
                      $messages[] = strtolower($edit->message);
                  }

                  //echo "<div class='alert alert-danger'>". $edit->message. "</div>";
                  echo "<div class='alert alert-danger'>". implode(', ', $messages). "</div>";
                }
              }
              else {
                echo "<div class='alert alert-danger'>Data ".implode(",",$empty)." harus diisi</div>";
              }
            }

          $provinces = $functions->getProvinces();
          $cities = $functions->getCities($data->province->pvid);
          $districts = $functions->getDistricts($data->province->pvid, $data->city->cid);
        ?>
        <div class="row">
          <div class="col-lg-12">
            <h1 class="page-header">
             <small>Sesuaikan Data RT</small>
             <a class="btn btn-primary pull-right btn-back" href="<?php echo $baseurl . "/app/detail_rt.php"; ?>"><i class="ion-android-arrow-back"></i></a>
           </h1>
         </div>
        </div>
        <form class="form" method="POST" enctype="multipart/form-data" action="detail_rt.php?page=edit" novalidate>
          <input type="hidden" name="neighbourhoodid" value="<?php echo $_COOKIE['rtid']; ?>">
          <div class="form-group">
            <label class="form-label">Nomor RT</label>
            <div class="input-group col-md-12">
              <span class="input-group-addon"><i class="fa fa-fw fa-map-signs"></i></span>
              <input value="<?php echo $data->rt ?>" type="number" maxlength="5" name="rt" id="rt" class="form-control" placeholder="Nomor RT" required />
            </div>
          </div>
          <div class="form-group">
            <label class="form-label">Nomor RW</label>
            <div class="input-group col-md-12">
              <span class="input-group-addon"><i class="fa fa-fw fa-map-signs"></i></span>
              <input value="<?php echo $data->rw ?>" type="number" maxlength="5" name="rw" id="rw" class="form-control" placeholder="Nomor RW" required />
            </div>
          </div>
          <div class="form-group">
            <label class="form-label">Provinsi</label>
            <div class="input-group col-md-12">
              <span class="input-group-addon"><i class="fa fa-fw fa-flag"></i></span>
              <label for="province_id" class="select-box">
                <select name="province_id" id="province_id" class="form-control" placeholder="Provinsi" required>
                  <option value="">Pilih Provinsi</option>
                  <?php foreach ($provinces as $key => $value): ?>
                    <option value="<?php echo $value->pvid; ?>" <?php echo ($data->province->pvid == $value->pvid) ? 'selected' : ''; ?>><?php echo $value->name ?></option>
                  <?php endforeach ?>
                </select>
              </label>
            </div>
          </div>
          <div class="form-group">
            <label class="form-label">Kabupaten / Kota</label>
            <div class="input-group col-md-12">
              <span class="input-group-addon"><i class="fa fa-fw fa-flag"></i></span>
              <label for="city_id" class="select-box">
                <select name="city_id" id="city_id" class="form-control" placeholder="Kabupaten" required>
                  <?php foreach ($cities as $key => $value): ?>
                    <option value="<?php echo $value->cid; ?>" <?php echo ($data->city->cid == $value->cid) ? 'selected' : ''; ?>><?php echo $value->name; ?></option>
                  <?php endforeach ?>
                </select>
              </label>
            </div>
          </div>
          <div class="form-group">
            <label class="form-label">Kecamatan</label>
            <div class="input-group col-md-12">
              <span class="input-group-addon"><i class="fa fa-fw fa-flag"></i></span>
              <label for="district_id" class="select-box">
                <select name="district_id" id="district_id" class="form-control" placeholder="Kecamatan" required>
                  <?php foreach ($districts as $key => $value): ?>
                    <option value="<?php echo $value->did; ?>" <?php echo ($data->district->did == $value->did) ? 'selected' : ''; ?>><?php echo $value->name; ?></option>
                  <?php endforeach ?>
                </select>
              </label>
            </div>
          </div>
          <div class="form-group">
            <label class="form-label">Desa / Kelurahan</label>
            <div class="input-group col-md-12">
              <span class="input-group-addon"><i class="fa fa-fw fa-flag"></i></span>
              <input value="<?php echo $data->village; ?>" type="text" maxlength="50" name="village" id="village" class="form-control" placeholder="Desa / Kelurahan" required />
            </div>
          </div>
          <div class="form-group">
              <label class="form-label">Cap RT / logo lingkungan</label>
              <div class="input-group col-md-12">
                  <label for="validation_doc_img">
                    <input type="file" name="validation_doc_img" id="validation_doc_img" class="form-control" placeholder="Stempel RT" accept="image/*;capture=camera" onclick="choosePhoto();" required/>
                  </label>
                  <input type="hidden" name="approval_photo" id="photo" value="" /><br />
                  <?php if($data->approval_photo == ""){ ?>
                  <span class="form-help text-center">Upload foto cap RT / logo lingkungan untuk mendapatkan validasi.</span>
                  <?php } ?>
                  <div id="files" class="files">
                      <img id="photo_preview" class="validation_doc_img_preview" data-original="<?php echo $data->approval_photo; ?>" />
                  </div>
              </div>
          </div>

          <!-- <div class="form-group">
            <label class="form-label">Negara</label>
            <div class="input-group col-md-12">
              <span class="input-group-addon"><i class="fa fa-fw fa-flag"></i></span> -->
              <input value="<?php echo $data->country ?>" type="hidden" maxlength="50" name="country" id="country" class="form-control" placeholder="Negara" required value="Indonesia" />
            <!-- </div>
          </div> -->
          <div class="form-group">
            <div class="input-group col-md-12">
              <button type="submit" class="btn-primary btn">Simpan</button>
            </div>
          </div>
        </form>

        <?php }
        elseif(!isset($_GET['page']) || @$_GET['page']=="browse"){
          $datart = $functions->getRT( $_COOKIE['rtid'] );
          $datart = $datart->data->rows{0};
          $param = array(
            'nid' => $_COOKIE['rtid'],
            'family_type' => "KK"
          );
          $warga = $functions->indexUser($param);
          $jumlah = count($warga);
        ?>
        <div class="wrapper-rt">
          <?php if($_COOKIE['kentongantype']=="rt"){ ?>
            <a href="<?php echo $baseurl . "/app/"; ?>detail_rt.php?page=edit" class="edit-detail"><i class="ion-compose"></i></a>
          <?php } ?>
          <div class="detail-rt">
            <h2><?php echo "RT ". $datart->rt . " / RW  " . $datart->rw . "<br />Desa / Kelurahan " . $datart->village . ", <br />" . $datart->district->name . ", " . $datart->city->name; ?></h2>
            <p><a href="<?php echo $baseurl . "/app/"; ?>detail_rt.php?page=list"><?php echo $jumlah; ?> Kepala Keluarga</a></p>
          </div>
        </div>
        <!-- Display timeline -->
        <div class="timeline" style="padding-top:">
          <div class="">
            <div id="main-content">
              <div class="row">
                <div class="col-lg-12">
                  <h1 class="page-header" style="padding-top: 10px">
                    <small>Aktivitas RT</small>
                    <?php if( $_COOKIE['kentongantype'] == "rt") { ?>
                    <a class="btn btn-primary pull-right btn-plus" href="new-status.php"><i class="ion-plus-circled"></i></a>
                    <?php } ?>
                  </h1>
                </div>
              </div>
              <div class="wrapper">
                <div class="main-content">
                  <section>
                    <ul class="media-list">
                      <?php require('activity-part.php'); ?>
                    </ul>
                  </section>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php
        }
        elseif($_GET['page']=="list"){
          $param = array(
            'nid' => $_COOKIE['rtid'],
            'family_type' => "KK"
          );
          $warga = $functions->indexUser($param);
        ?>
        <div class="row">
          <div class="col-lg-12">
            <h1 class="page-header">
             <small>Daftar Kepala Keluarga</small>
             <a class="btn btn-primary pull-right btn-back" href="<?php echo $baseurl . "/app/detail_rt.php"; ?>"><i class="ion-android-arrow-back"></i></a>
           </h1>
         </div>
        </div>
        <table class="table data-table">
          <thead>
            <tr>
              <th>Nama</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $seq = 1;
              foreach($warga as $data){
            ?>
              <tr>
                  <td>
                    <div class='img-container'><img data-original="<?php echo ($data->photo != '') ? $apiurl.$data->photo : 'http://i.imgur.com/kfyG0il.png'; ?>" alt='' /></div>
                    <span class='card-name-label'><?php echo ($data->name <> "") ? $data->name : "-" ;?></span>
                    <span class='card-phone-label'><?php echo ($data->phone <> "") ? "+628xxxxxx" . substr($data->phone,-4) : "-"; ?></span>
                  </td>
                </tr>
            <?php
              }
            ?>
          </tbody>
        </table>
        <?php 
        }
        ?>
      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
<script type="text/javascript">
    function showAndroidToast(toast) {
        Android.showToast(toast);
    }
    function setFilePath(file) {
        console.log( file );
        document.getElementById('photo').value = file;
        Android.showToast(file);
    }
    function setFileUri(uri) {
        document.getElementById('photo_preview').src = uri;
        Android.showToast(uri);
    }
    function choosePhoto() {
        var file = Android.choosePhoto();
    }
</script>
<?php require("../footer.php"); ?>
