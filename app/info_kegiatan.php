<?php require("../header.php"); ?>
<?php 
  $min_year = date('Y');
  $max_year = date('Y')+10;
  $default_date = date('Y-m-d');

  $time = '';
  function gettime( $value = null){
    $time = '';
    for ($h=0; $h < 24; $h++) { 
      for ($m=0; $m < 60; $m+=15) { 
        $selected = "";
        if(isset($value)){
          $selected = ( sprintf("%02d", $h). ':'. sprintf("%02d", $m) == $value ) ? "selected" : null;
        }
        $time .= '<option '.$selected.' value="'. sprintf("%02d", $h). ':'. sprintf("%02d", $m). '">'. sprintf("%02d", $h). ':'. sprintf("%02d", $m). '</option>';
      }
    }
    return $time;
  }
?>
<div id="page-wrapper">
  <div class="container-fluid">
    <div id="main-content">

      <div class="card-content">
        <?php
        if (@$_GET['page'] == "add"){
          $defValue = array(
            'name' => null,
            'start_date' => null,
            'end_date' => null,
            'location' => null,
            'type' => null,
            'description' => null,
          );
          if(count($_POST)>0){
            $defValue += $_POST;
            $_POST['start_date'] = $_POST['startdate']. ' '. $_POST['start_date'];
            $_POST['end_date'] = $_POST['enddate']. ' '. $_POST['end_date'];

            $valid = TRUE;
            if (strtotime($_POST['start_date']) > strtotime($_POST['end_date'])) {
              $valid = FALSE;
              echo "<div class='alert alert-danger'>Tanggal jam mulai lebih besar dari tanggal jam selesai</div>";
            }
            if ($valid) {
              $_POST['end_date'] = date('Y-m-d H:i', strtotime($_POST['end_date']));
              $_POST['start_date'] = date('Y-m-d H:i', strtotime($_POST['start_date']));
              unset($_POST['select-enddate_']);
              unset($_POST['select-startdate_']);
              unset($_POST['startdatetime']);
              unset($_POST['enddatetime']);

              $insert = $functions->createEvent($_POST);
              if($insert->status){
                //$notifid = mysqli_insert_id($mysqli);
                $message = "Ada Acara ".$_POST['name']." di ".$_POST['location']." pada ".$_POST['start_date'];
                if (isset($_POST['end_date'])){
                  $message .= " sampai ".$_POST['end_date'];
                }
                //$pushnotif->push(array("alert" => $message,"link" => "warga/info_kegiatan.php".$tokenurl."&page=view&id=".$notifid ), array("rt-".$_COOKIE['rtid']));
                echo "<div class='alert alert-success'>Berhasil Disimpan</div>";
              }
              else {
                $messages = array();
                if ($insert->message == 'Validation Error') {
                    foreach ($insert->data->rows as $key => $value) {
                        foreach ($value as $message) {
                            $messages[] = strtolower($message);
                        }
                    }
                } else {
                    $messages[] = strtolower($insert->message);
                }
                echo "<div class='alert alert-danger'>Data kegiatan gagal disimpan: ". implode(', ', $messages). "</div>";
              }
            }
          }
          ?>
          <div class="row">
            <div class="col-lg-12">
              <h1 class="page-header">
                <small>Info Kegiatan RT</small>
                <a class="btn btn-primary pull-right btn-back" href="info_kegiatan.php"><i class="ion-android-arrow-back"></i></a>
              </h1>
            </div>
          </div>
          <form class="form" enctype="multipart/form-data" method="POST" action="info_kegiatan.php?page=add">
            <div class="form-group">
              <label class="form-label">Nama Kegiatan</label>
              <div class="input-group col-md-12">
                <input type="text" name="name" value="<?php echo $defValue['name'] ?>" id="name" class="form-control" placeholder="Nama Kegiatan" required />
                <input type="hidden" name="neighbourhood" id="neighbourhood" class="form-control" value="<?php echo $_COOKIE['rtid'];?>" />
              </div>
            </div>
            <div class="form-group">
              <label class="form-label">Tanggal Jam Mulai</label>
              <div class="input-group col-md-12 info date-time">
                <input type="hidden" name="startdate" data-value="<?php echo date("Y-m-d"); ?>" id="select-startdate" class="form-control datetime" placeholder="Tanggal Mulai">
                <label for="startdatetime" class="select-box" style="width:21%;margin-left:2%;">
                <select id="startdatetime" name="start_date" class="time">
                  <?php echo gettime($defValue['start_date']);?>
                </select>
                </label>
            </div>
            <div class="form-group">
              <label class="form-label">Tanggal Jam Selesai</label>
              <div class="input-group col-md-12 info date-time">
                <input type="hidden" name="enddate" data-value="<?php echo date("Y-m-d"); ?>" id="select-enddate" class="form-control datetime" placeholder="Tanggal Selesai">
                <label for="enddatetime" class="select-box" style="width:21%;margin-left:2%;">
                <select id="enddatetime" name="end_date" class="time">
                  <?php echo gettime($defValue['start_date']);?>
                </select>
                </label>
            </div>
            <div class="form-group">
              <label class="form-label">Lokasi</label>
              <div class="input-group col-md-12">
                <textarea name="location" id="location" class="form-control" placeholder="Lokasi" value="<?php echo $defValue['locaton'] ?>" required></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label">Jenis Kegiatan</label>
              <div class="input-group col-md-12">
                <input type="text" name="type" id="eventtype" class="form-control" placeholder="Jenis Kegiatan" value="<?php echo $defValue['type'] ?>" required />
              </div>
            </div>
            <div class="form-group">
              <label class="form-label">Keterangan</label>
              <div class="input-group col-md-12">
                <textarea name="description" id="description" class="form-control" placeholder="Keterangan" value="<?php echo $defValue['description'] ?>" required></textarea>
              </div>
            </div>
            <div class="form-action">
              <div class="input-group col-md-12">
                <button type="submit" class="btn-primary btn">Simpan</button>
              </div>
            </div>
          </form>


          <?php 
          }
          elseif (@$_GET['page'] == "edit"){
            if(count($_POST)>0){
              $_POST['start_date'] = $_POST['startdate']. ' '. $_POST['start_date'];
              $_POST['end_date'] = $_POST['enddate']. ' '. $_POST['end_date'];

              $_POST['end_date'] = date('Y-m-d H:i', strtotime($_POST['end_date']));
              $_POST['start_date'] = date('Y-m-d H:i', strtotime($_POST['start_date']));
              unset($_POST['select-enddate_']);
              unset($_POST['select-startdate_']);
              unset($_POST['startdatetime']);
              unset($_POST['enddatetime']);

              $update = $functions->updateEvent($_GET['id'], $_POST);
              if($update->status){
                $message = "Perubahan Acara ".$_POST['name']." di ".$_POST['location']." pada ".$_POST['start_date'];
                if (isset($_POST['end_date'])){
                  $message .= " sampai ".$_POST['end_date'];
                }
                //$pushnotif->push(array("alert" => $message, "link" => "warga/info_kegiatan.php?page=view&id=".$_GET['id']), array("rt-".$_COOKIE['rtid']));
                redirect("info_kegiatan.php");
                // echo "<script type='text/javascript'>window.location.href='info_kegiatan.php';</script>";
                //echo "<div class='alert alert-success'>Berhasil Disimpan</div>";
              }
              else {
                $messages = array();
                if ($update->message == 'Validation Error') {
                    foreach ($update->data->rows as $key => $value) {
                        foreach ($value as $message) {
                            $messages[] = strtolower($message);
                        }
                    }
                } else {
                    $messages[] = strtolower($update->message);
                }
                echo "<div class='alert alert-danger'>Data kegiatan gagal disimpan: ". implode(', ', $messages). "</div>";
              }
            }
            $event = $functions->getEvent($_GET['id']);
            $data = $event->data->rows[0];

          ?>
          <div class="row">
            <div class="col-lg-12">
              <h1 class="page-header">
                <small>Edit Info Kegiatan RT</small>
                <a class="btn btn-primary pull-right btn-back" href="info_kegiatan.php"><i class="ion-android-arrow-back"></i></a>
              </h1>
            </div>
          </div>
          <form class="form" enctype="multipart/form-data" method="POST" action="info_kegiatan.php?page=edit&id=<?php echo $_GET['id']; ?>">
            <div class="form-group">
              <label class="form-label">Nama Kegiatan</label>
              <div class="input-group col-md-12">
                <input type="text" value="<?php echo $data->name ?>" name="name" id="name" class="form-control" placeholder="Nama Kegiatan" required />
                <input type="hidden" name="neighbourhood" id="neighbourhood" class="form-control" value="<?php echo $_COOKIE['rtid'];?>" />
              </div>
            </div>
            <div class="form-group">
              <label class="form-label">Tanggal Jam Mulai</label>
              <div class="input-group col-md-12 edit-info date-time">
                <input type="hidden" name="startdate" data-value="<?php echo date("Y-m-d",strtotime($data->start_date)); ?>" id="select-startdate" class="form-control datetime" placeholder="Tanggal Mulai">
                <label for="startdatetime" class="select-box" style="width: 21%;margin-left: 2%;">
                <select id="startdatetime" name="start_date" class="time">
                  <?php echo gettime( date("H:i",strtotime($functions->getDateTime($data->start_date))) );?>
                </select>
                </label>
              </div> 
            </div>
            <div class="form-group">
              <label class="form-label">Tanggal Jam Selesai</label>
              <div class="input-group col-md-12 edit-info date-time">
                <input type="hidden" name="enddate" data-value="<?php echo date("Y-m-d",strtotime($data->end_date)); ?>" id="select-enddate" class="form-control datetime" placeholder="Tanggal Selesai">
                <label for="enddatetime" class="select-box" style="width:21%;margin-left:2%;">
                  <select id="enddatetime" name="end_date" class="time">
                    <?php echo gettime( date("H:i",strtotime($functions->getDateTime($data->end_date))) );?>
                  </select>
                </label>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label">Lokasi</label>
              <div class="input-group col-md-12">
                <textarea name="location" id="location" class="form-control" placeholder="Lokasi" required><?php echo $data->location ?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label">Jenis Kegiatan</label>
              <div class="input-group col-md-12">
                <input type="text" value="<?php echo $data->type ?>" name="type" id="eventtype" class="form-control" placeholder="Jenis Kegiatan" required />
              </div>
            </div>
            <div class="form-group">
              <label class="form-label">Keterangan</label>
              <div class="input-group col-md-12">
                <textarea name="description" id="description" class="form-control" placeholder="Keterangan" required><?php echo $data->description ?></textarea>
              </div>
            </div>
            <div class="form-action">
              <div class="input-group col-md-12">
                <button type="submit" class="btn-primary btn">Simpan</button>
              </div>
            </div>
          </form>


          <?php }
            elseif (@$_GET['page'] == "delete"){
              $insert = $functions->deleteEvent($_GET['id']);
              redirect("info_kegiatan.php");
            }
            elseif (@$_GET['page'] == "view" || @$_GET['page'] == "detail"){
              $event = $functions->getEvent($_GET['id']);
              $data = $event->data->rows[0];
          ?>
          <div class="row">
            <div class="col-lg-12">
              <h1 class="page-header">
                <small>Detail Info Kegiatan</small>
                <a class="btn btn-primary pull-right btn-back" href="info_kegiatan.php"><i class="ion-android-arrow-back"></i></a>
              </h1>
            </div>
          </div>
          <form class="form" enctype="multipart/form-data" method="POST" action="info_kegiatan.php?page=edit&id=<?php echo $_GET['id']; ?>">
            <div class="form-group">
              <label class="form-label">Nama Kegiatan</label>
              <div class="input-group col-md-12">
                <?php echo $data->name ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label">Tanggal Jam Mulai</label>
              <div class="input-group col-md-12">
                <?php echo date("d-M-Y | H:i:s",strtotime($functions->getDateTime($data->start_date))); ?>
              </div> 
            </div>
            <div class="form-group">
              <label class="form-label">Tanggal Jam Selesai</label>
              <div class="input-group col-md-12">
                <?php echo date("d-M-Y | H:i:s",strtotime($functions->getDateTime($data->end_date))); ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label">Lokasi</label>
              <div class="input-group col-md-12">
                <?php echo $data->location ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label">Jenis Kegiatan</label>
              <div class="input-group col-md-12">
                <?php echo $data->type ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label">Keterangan</label>
              <div class="input-group col-md-12">
                <?php echo $data->description ?>
              </div>
            </div>
          </form>

          <?php
            }
            else {

            $indexEvents = $functions->indexEvent();
            $data = $indexEvents->data->rows;
            ?>

            <!-- Page Heading -->
            <div class="row">
              <div class="col-lg-12">
                <h1 class="page-header">
                  <small>Info Kegiatan RT</small>
                  <?php if($_COOKIE['kentongantype'] == 'rt') { ?>
                  <a class="btn btn-primary pull-right btn-plus" href="info_kegiatan.php?page=add">
                     <i class="ion-plus-circled"></i>
                  </a>
                  <?php } ?>
                </h1>
              </div>
            </div>
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Nama</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach ($data as $row) { ?>
                <tr>
                  <td>
                    <?php 
                      $name = (strlen($row->name) > 20 ) ? substr($row->name, 0, 20) . "..." : $row->name ;
                    ?>
                    <span class="card-name-label"><strong><?php echo  $name ?></strong></span>
                    <span class="card-mobile-label"><small>Waktu: <?php echo date("d M Y", strtotime($functions->getDateTime($row->start_date))) . " - " . date("H:i", strtotime($functions->getDateTime($row->start_date))); ?></small></span>
                    <span class="card-mobile-label"><small>Lokasi: <?php echo $row->location; ?></small></span>
                  </td>

                  <td style="width:30px;" class="text-right">
                  <?php if($_COOKIE['kentongantype'] == "rt"){ ?>
                    <a href="info_kegiatan.php?page=option&id=<?php echo $row->eid; ?>" class="list-option"><span class="ion-chevron-down"></span></a>
                  <?php } else { ?>
                    <a href="info_kegiatan.php?page=view&id=<?php echo $row->eid; ?>" class="list-option"><span class="ion-information-circled"></span></a>
                  <?php } ?>
                  </td>
                </tr>
                <?php } ?>
              </tbody>

            </table>

            <?php
          }
          ?>

        </div>
      </div>
    </div>
    <!-- /.container-fluid -->

  </div>
  <!-- /#page-wrapper -->
  <script type="text/javascript">
    var minYear = "<?php echo $min_year; ?>";
    var maxYear = "<?php echo $max_year; ?>";
  </script>
  <?php require("../footer.php"); ?>