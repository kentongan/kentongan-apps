<?php require("../header.php");
    $max_year = date("Y");
    $default_date = date("Y-m-d");
    $familyType = $functions->familyType()->data->rows;  
    $datakk = $functions->getUser($_COOKIE['peopleid']);
?>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div id="main-content">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation"><a href="biodata_pribadi.php">Biodata Pribadi</a></li>
                    <li role="presentation" class="active"><a href="keluarga.php">Keluarga</a></li>
                </ul>
                <div class="card-content">
                    <?php
                    if(@$_GET['page']=="browse" || !isset($_GET['page'])){
                        $data = $functions->getFamily($datakk[0]->family_id, $_COOKIE['peopleid'])->data->rows;
                        $family = array("" => "Belum diset");
                        foreach($familyType as $families){
                            $family[$families->ftid] = $families->name;
                        }
                    ?>
                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                <small>Daftar Keluarga Anda</small>
                                <a class="btn btn-primary pull-right btn-plus" href="keluarga.php?page=add"><i class="ion-plus-circled"></i></a>
                            </h1>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="biodata">
                            <table class="table table-bordered data-table">
                                <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($data as $row) { ?>
                                    <tr>
                                        <td>
                                            <div class="img-container"><img data-original="<?php echo ($row->photo != '') ? $apiurl.$row->photo : 'http://i.imgur.com/kfyG0il.png' ?>" alt="" /></div>
                                            <?php if( strtotime($row->updated) > strtotime($row->created)){
                                                echo "<img src='".$baseurl."/assets/images/favicons/favicon-16x16.png' class='kentongan-sign'>";
                                            } ?>
                                            <span class="card-name-label"><?php echo $row->name ?></span>
                                            <span class="card-phone-label"><?php echo $row->phone ?></span>
                                        </td>
                                        <td style="width:30px;" class="text-right">
                                            <a href="keluarga.php?page=option&id=<?php echo $row->pid; ?>" class="list-option"><span class="ion-chevron-down"></span></a>
                                            <!-- <div class="btn-group">
                                              <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></button>
                                              <ul class="dropdown-menu action-dropdown">
                                                <li><a class='btn btn-success' title="Edit" href="warga.php?page=edit&id=<?php echo $row->pid; ?>"><i class="fa fa-edit"></i></a></li>
                                                <li><a class='btn btn-success' title="Keluarga" href="warga.php?page=keluarga&id=<?php echo $row->pid; ?>"><i class="fa fa-users"></i></a></li>
                                                <li><a class='btn btn-danger confirm' title="Delete" href="warga.php?page=delete&id=<?php echo $row->pid; ?>"><i class="fa fa-trash"></i></a></li>
                                              </ul>
                                            </div> -->
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>

                            </table>
                        <?php }
                        if (@$_GET['page'] == "add"){
                        ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1 class="page-header">
                                        <small>Tambah Data Keluarga</small>
                                        <a class="btn btn-primary pull-right btn-back" href="keluarga.php"><i class="ion-android-arrow-back"></i></a>
                                    </h1>
                                </div>
                            </div>
                            <?php
                                if(count($_POST) > 0) {
                                    unset($_POST['date_']);

                                    $datakk = $functions->getUser($_COOKIE['peopleid']);
                                    $_POST['neighbourhood_id'] = $datakk[0]->neighbourhood->nid;
                                    $_POST['family_id'] = $datakk[0]->family_id;
                                    $_POST['role'] = "wr";
                                    // $_POST['status'] = "1";

                                    $insert = $functions->insertPeople($_POST);
                                    if($insert->status == true){
                                        echo "<div class='alert alert-success'>".$insert->message."</div>";
                                        $_POST=array();
                                    }
                                    else {
                                        $messages = array();
                                        if ($insert->message == 'Validation Error') {
                                            foreach ($insert->data->rows as $key => $value) {
                                                foreach ($value as $message) {
                                                    $messages[] = strtolower($message);
                                                }
                                            }
                                        } else {
                                            $messages[] = strtolower($insert->message);
                                        }
                                        echo "<div class='alert alert-danger'>Anggota keluarga gagal ditambahkan: ". implode(', ', $messages)."</div>";
                                    }
                                }
                            ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <form class="form" method="POST" enctype="multipart/form-data" action="keluarga.php?page=add">
                                        <input type="hidden" name="password" value="" />
                                        <div class="form-group">
                                            <label class="form-label">Nama</label>
                                            <div class="input-group col-md-12">
                                                <input value="<?php echo @$_POST['name']; ?>" type="text" name="name" id="name" class="form-control" placeholder="Nama" maxlength="75"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Alamat</label>
                                            <div class="input-group col-md-12">
                                                <textarea name="address" id="address" class="form-control" placeholder="Alamat"><?php echo @$_POST['address']; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Email</label>
                                            <div class="input-group col-md-12">
                                                <input value="<?php echo @$_POST['email']; ?>" type="email" name="email" id="email" class="form-control" placeholder="Email" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">No. Telepon</label>
                                            <div class="input-group col-md-12">
                                                <input value="<?php echo @$_POST['phone']; ?>" type="text" name="phone" id="phone" class="form-control" placeholder="No. Telepon" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Tempat Lahir</label>
                                            <div class="input-group col-md-12">
                                                <input value="<?php echo @$_POST['birth_place']; ?>" type="text" name="birth_place" id="birth_place" class="form-control" placeholder="Tempat Lahir" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Tanggal Lahir</label>
                                            <div class="input-group col-md-12">
                                                <input value="<?php echo @$_POST['birth_date']; ?>" id="birth_date" name="birth_date" class="form-control date" data-format="dd/MM/yyyy hh:mm:ss" placeholder="Tanggal Lahir" type="date" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Jenis Kelamin</label>
                                            <div class="input-group col-md-12">
                                                <select name="gender" class="form-control" id="gender">
                                                    <option <?php echo @$_POST['gender']=="L" ? "selected" : null ; ?> value="L" >Laki Laki</option>
                                                    <option <?php echo @$_POST['gender']=="P" ? "selected" : null ; ?> value="P" >Perempuan</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group col-md-12">
                                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                <label for="family_type">Status di Keluarga</label>
                                                <select name="family_type" id="family_type" class="form-control">
                                                    <?php
                                                        foreach($familyType as $family){
                                                            $selected = ($_POST['family_type'] == $family->ftid) ? "selected" : null ;
                                                            echo "<option ".$selected." value='".$family->ftid."'>".$family->name."</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group col-md-12">
                                                <label for="education">Tingkat Pendidikan</label>
                                                <select name="education" class="form-control" id="educationlevel">
                                                    <option value="Tidak Sekolah" <?php echo (@$_POST['education'] == 'Tidak Sekolah') ? 'selected' : ''; ?>>Tidak Sekolah</option>
                                                    <option value="TK" <?php echo (@$_POST['education'] == 'TK') ? 'selected' : ''; ?>>TK</option>
                                                    <option value="SD" <?php echo (@$_POST['education'] == 'SD') ? 'selected' : ''; ?>>SD</option>
                                                    <option value="SMP" <?php echo (@$_POST['education'] == 'SMP') ? 'selected' : ''; ?>>SMP</option>
                                                    <option value="SMA/SMK" <?php echo (@$_POST['education'] == 'SMA/SMK') ? 'selected' : ''; ?>>SMA/SMK</option>
                                                    <option value="D1" <?php echo (@$_POST['education'] == 'D1') ? 'selected' : ''; ?>>D1</option>
                                                    <option value="D2" <?php echo (@$_POST['education'] == 'D2') ? 'selected' : ''; ?>>D2</option>
                                                    <option value="D3" <?php echo (@$_POST['education'] == 'D3') ? 'selected' : ''; ?>>D3</option>
                                                    <option value="S1" <?php echo (@$_POST['education'] == 'S1') ? 'selected' : ''; ?>>S1</option>
                                                    <option value="S2" <?php echo (@$_POST['education'] == 'S2') ? 'selected' : ''; ?>>S2</option>
                                                    <option value="S3" <?php echo (@$_POST['education'] == 'S3') ? 'selected' : ''; ?>>S3</option>
                                                </select>
                                            </div>
                                        </div>
                                        <?php /*
                                        <div class="form-group">
                                            <label class="form-label">No. KTP / NIK</label>
                                            <div class="input-group col-md-12">
                                                <input value="<?php echo @$_POST['card_id']; ?>" type="text" name="card_id" id="card_id" class="form-control" placeholder="No. KTP / NIK (optional)" />
                                            </div>
                                        </div>
                                        */ ?>
                                        <div class="form-group">
                                            <div class="input-group col-md-12">
                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                <label for="#">Foto</label>
                                                <label for="photo_img">
                                                <input type="file" name="photo_img" id="photo_img" class="form-control" placeholder="Foto" onclick="choosePhoto();" />
                                                </label>
                                                <input type="hidden" name="photo" id="photo" />
                                                <div id="files" class="files"><img id="photo_preview" data-original="<?php echo $data->photo; ?>" /></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group col-md-12">
                                                <button type="submit" class="btn-primary btn">Simpan</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        <?php }
                        if (@$_GET['page'] == "delete"){
                            $delete = $functions->deleteUser($_GET['id']);
                            redirect("keluarga.php?page=browse");
                            // echo '<script type="text/javascript">window.location.href="keluarga.php?page=browse";</script>';
                        }
                        if(@$_GET['page'] == "edit"){
                            if( count($_POST) > 0){
                                $update = $functions->updateUser($_GET['id'],$_POST);
                                if($update->code == 200){
                                    echo "<div class='alert alert-success'>".$update->message."</div>";
                                }
                                else {
                                    $messages = array();
                                    if ($update->message == 'Validation Error') {
                                        foreach ($update->data->rows as $key => $value) {
                                            foreach ($value as $message) {
                                                $messages[] = strtolower($message);
                                            }
                                        }
                                    } else {
                                        $messages[] = strtolower($update->message);
                                    }
                                    echo "<div class='alert alert-danger'>Data keluarga gagal disimpan: ". implode(', ', $messages). "</div>";
                                }
                            }
                            if(!isset($_GET['id'])){
                                echo "Invalid parameter..!";
                                exit;
                            }
                            $data = $functions->getUser($_GET['id']);
                            if(!isset($data)){
                                echo "Data Not Found";
                                exit;
                            }
                            $data=$data[0];
                        ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1 class="page-header">
                                        <small>Sesuaikan Data Keluarga</small>
                                        <a class="btn btn-primary pull-right btn-back" href="keluarga.php"><i class="ion-android-arrow-back"></i></a>
                                    </h1>
                                </div>
                            </div>
                            <form class="form" method="POST" enctype="multipart/form-data" action="keluarga.php?page=edit&id=<?php echo $_GET['id']; ?>">
                                <input type="hidden" name="password" value="" />
                                <div class="form-group">
                                    <label class="form-label">Nama</label>
                                    <div class="input-group col-md-12">
                                        <input value="<?php echo @$data->name; ?>" type="text" name="name" id="name" class="form-control" placeholder="Nama" maxlength="75"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Email</label>
                                    <div class="input-group col-md-12">
                                        <input value="<?php echo @$data->email; ?>" type="email" name="email" id="email" class="form-control" placeholder="Email" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">No. Telepon</label>
                                    <div class="input-group col-md-12">
                                        <input value="<?php echo @$data->phone; ?>" type="text" name="phone" id="phone" class="form-control" placeholder="No. Telepon" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Tempat Lahir</label>
                                    <div class="input-group col-md-12">
                                        <input value="<?php echo @$data->birth_place; ?>" type="text" name="birth_place" id="birth_place" class="form-control" placeholder="Tempat Lahir" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Tanggal Lahir</label>
                                    <div class="input-group col-md-12">
                                        <input value="<?php echo @$data->birth_date; ?>" type="date" name="birth_date" id="birth_date" class="form-control date" placeholder="Tanggal lahir" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Jenis Kelamin</label>
                                    <div class="input-group col-md-12">
                                        <select name="gender" class="form-control" id="gender">
                                            <option <?php echo @$data->gender=="L" ? "selected" : null ; ?> value="L" >Laki Laki</option>
                                            <option <?php echo @$data->gender=="P" ? "selected" : null ; ?> value="P" >Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group col-md-12">
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                        <label for="family_type">Status di Keluarga</label>
                                        <select name="family_type" id="family_type" class="form-control">
                                            <?php
                                                foreach($familyType as $family){
                                                    $selected = ($data->family_type == $family->ftid) ? "selected" : null ;
                                                    echo "<option ".$selected." value='".$family->ftid."'>".$family->name."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group col-md-12">
                                        <label for="education">Tingkat Pendidikan</label>
                                        <!--<input value="<?php /*echo @$data->education; */?>" type="text" name="education" id="education" class="form-control" placeholder="Tingkat Pendidikan" />-->
                                        <select name="education" class="form-control" id="educationlevel">
                                            <option value="Tidak Sekolah" <?php echo (@$data->education == 'Tidak Sekolah') ? 'selected' : ''; ?>>Tidak Sekolah</option>
                                            <option value="TK" <?php echo (@$data->education == 'TK') ? 'selected' : ''; ?>>TK</option>
                                            <option value="SD" <?php echo (@$data->education == 'SD') ? 'selected' : ''; ?>>SD</option>
                                            <option value="SMP" <?php echo (@$data->education == 'SMP') ? 'selected' : ''; ?>>SMP</option>
                                            <option value="SMA/SMK" <?php echo (@$data->education == 'SMA/SMK') ? 'selected' : ''; ?>>SMA/SMK</option>
                                            <option value="D1" <?php echo (@$data->education == 'D1') ? 'selected' : ''; ?>>D1</option>
                                            <option value="D2" <?php echo (@$data->education == 'D2') ? 'selected' : ''; ?>>D2</option>
                                            <option value="D3" <?php echo (@$data->education == 'D3') ? 'selected' : ''; ?>>D3</option>
                                            <option value="S1" <?php echo (@$data->education == 'S1') ? 'selected' : ''; ?>>S1</option>
                                            <option value="S2" <?php echo (@$data->education == 'S2') ? 'selected' : ''; ?>>S2</option>
                                            <option value="S3" <?php echo (@$data->education == 'S3') ? 'selected' : ''; ?>>S3</option>
                                        </select>
                                    </div>
                                </div>
                                <?php /*
                                <div class="form-group">
                                    <label class="form-label">No. KTP / NIK</label>
                                    <div class="input-group col-md-12">
                                        <input value="<?php echo @$data->card_id; ?>" type="text" name="card_id" id="card_id" class="form-control" placeholder="No. KTP / NIK (optional)" />
                                    </div>
                                </div>
                                */ ?>
                                <div class="form-group">
                                    <div class="input-group col-md-12">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <label for="#">Foto</label>
                                        <label for="photo_img">
                                        <input type="file" name="photo_img" id="photo_img" class="form-control" placeholder="Foto" onclick="choosePhoto();" />
                                        </label>
                                        <input type="hidden" name="photo" id="photo" />
                                        <div id="files" class="files"><img id="photo_preview" data-original="<?php echo $data->photo; ?>" /></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group col-md-12">
                                        <button type="submit" class="btn-primary btn">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        <?php
                        }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->
<script type="text/javascript">
    var maxYear = "<?php date('Y'); ?>";
    var defaultDate = "<?php echo @$data->birth_date; ?>";
</script>
<script type="text/javascript">
    function showAndroidToast(toast) {
        Android.showToast(toast);
    }
    function setFilePath(file) {
        document.getElementById('photo').value = file;
        Android.showToast(file);
    }
    function setFileUri(uri) {
        document.getElementById('photo_preview').src = uri;
        Android.showToast(uri);
    }
    function choosePhoto() {
        var file = Android.choosePhoto();
        // window.alert("file = " + file);
    }
</script>
<?php require("../footer.php"); ?>