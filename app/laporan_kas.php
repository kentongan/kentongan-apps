<?php require("../header.php"); ?>
<?php
  $pid = $_COOKIE['peopleid'];
  $data = $functions->getUser($pid);
  $user = $data[0];
?>
<div id="page-wrapper">
  <div class="container-fluid">
    <div id="main-content">
      <div class="card-content">
        <?php
        if(@$_GET['page'] == "browse" || !isset($_GET['page'])){
        ?>

        <!-- Page Heading -->
        <?php if($user->role == 'rt'): ?>
        <div class="row">
          <div class="col-lg-12">
            <h1 class="page-header">
              <small>Laporan Kas RT</small>
              <?php if($_COOKIE['kentongantype'] == 'rt'){ ?>
              <a class="btn btn-primary pull-right btn-plus" href="laporan_kas.php?page=add"><i class="ion-plus-circled"></i></a>
              <?php } ?>
            </h1>
          </div>
        </div>
        <?php endif; ?>

        <div class="search">
          <form class="form" method="POST" action="laporan_kas.php">
            <div class="form-group kas_rt col-xs-6">
              <div class="input-group">
                <label for="month" class="select-box">
                  <select name="month" class="form-control" id="month">
                    <?php
                    $months = $functions->getmonths();
                    $curmonth = ($_POST['month'] != "") ? $_POST['month'] : date("m");
                    foreach($months as $key => $value){
                      $selected = ($curmonth == $key) ? "selected" : null;
                      echo "<option ".$selected." value='".$key."'>".$value."</option>";
                    }
                    ?>
                  </select>
                </label>
              </div>
            </div>
            <div class="form-group kas_rt col-xs-6">
              <div class="input-group">
                <label for="year" class="select-box">
                  <select name="year" class="form-control" id="year">
                    <?php
                    $years = $functions->getyears();
                    $curyear = ($_POST['year'] != "") ? $_POST['year'] : date("Y");
                    foreach($years as $key => $value){
                      $selected = ($curyear == $key) ? "selected" : null;
                      echo "<option ".$selected." value='".$key."'>".$value."</option>";
                    }
                    ?>
                  </select>
                </label>
              </div>
            </div>
            <button class="hidden" type="submit" id="submit"></button>
          </form>
        </div>

        <?php
        if ((int)$curmonth == '1') {
          $curYearSaldoAwal = $curyear-1;
          $curMonthSaldoAwal = 12;
        } else {
          $curYearSaldoAwal = $curyear;
          $curMonthSaldoAwal = $curmonth-1;
        }
        $indexFinances = $functions->indexFinance(array('month' => $curMonthSaldoAwal, 'year' => $curYearSaldoAwal, 'order' => 'fid:DESC'));
        $saldoawal = 0;
        if (count($indexFinances->data->rows)) $saldoawal = $indexFinances->data->rows[0]->total_amount;

        //$saldoawal = $kentongan->select("finances",array('neighbourhood' => $_COOKIE['rtid']), " AND CONCAT(YEAR(transactiondate),MONTH(transactiondate)) < '". $curyear . $curmonth ."' ORDER BY transactiondate DESC LIMIT 1");
        $indexFinances = $functions->indexFinance(array('month' => $curmonth, 'year' => $curyear, 'order' => 'fid:desc'));
        $data = $indexFinances->data->rows;
        ?>
        <table class="table table-condensed data-table table-kas" style="top:0">
          <thead>
          <tr>
            <th>Keterangan</th>
            <th style="width:100px;">Nominal</th>
          </tr>
          </thead>
          <tbody>
          <tr class="firstamount">
            <?php $class = ($saldoawal > 0) ? "text-success" : "text-danger";?>
            <?php $class = ($saldoawal == 0) ? "text" : $class;?>
            <td><span>Saldo Awal</span></td>
            <td class="text-right"><span class="<?php echo $class ?>"><?php echo accounting_format($saldoawal, 0); ?></span></td>
          </tr>
          <?php
          $total = 0;
          //$months = $kentongan->getmonths();

          foreach ($data as $row) {
            $class = ($row->type == "I") ? "text-success" : "text-danger";?>
            <tr id="<?php echo $row->fid; ?>" class="detail_kas">
              <td><span class="<?php echo $class; ?>"><?php echo $row->description; ?><br /><?php echo date("d M Y", strtotime($row->created)) ; ?></span></td>
              <td class="text-right"><span class="<?php echo $class ?>"><?php
                  if($row->type == "I"){
                    echo accounting_format($row->amount,0);
                  }
                  else {
                    echo accounting_format(($row->amount * -1), 0);
                  }
                  ?></span></td>
            </tr>
            <?php
            $row = reset($data);
            $total = $row->total_amount;
          }
          ?>
          <tr class="lastamount">
            <?php $class = ($total > 0) ? "text-success" : "text-danger"; ?>
            <?php $class = ($total == 0) ? "text" : $class; ?>
            <td><span>Saldo Akhir</span></td>
            <td class="text-right"><span class="<?php echo $class ?>"><?php echo accounting_format($total,0); ?></span></td>
          </tr>
          </tbody>

        </table>
      </div>
    </div>
    <?php
    }
    if(@$_GET['page'] == "add" && $user->role == 'rt'){
      if(count($_POST) > 0){
        $insert = $functions->createFinance($_POST);
        if($insert->status){
          echo "<div class='alert alert-success'>Laporan kas tersimpan, silahkan lihat di rekap</div>";
        }
        else {
          $messages = array();
          if ($insert->message == 'Validation Error') {
              foreach ($insert->data->rows as $key => $value) {
                  foreach ($value as $message) {
                      $messages[] = strtolower($message);
                  }
              }
          } else {
              $messages[] = strtolower($insert->message);
          }
          echo "<div class='alert alert-danger'>Laporan kas gagal disimpan: ". implode(', ', $messages). "</div>";
        }
      }
      ?>
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">
            <small>Pemasukan/Pengeluaran Kas RT</small>
            <a class="btn btn-primary pull-right btn-back" href="laporan_kas.php"><i class="ion-android-arrow-back"></i></a>
          </h1>
        </div>
      </div>
      <form method="POST" class="form" enctype="multipart/form-data" action="laporan_kas.php?page=add">
        <div class="form-group">
          <label class="form-label">Pemasukan / Pengeluaran</label>
          <div class="input-group col-md-12">
            <select name="type" class="form-control">
              <option value="I">Pemasukan</option>
              <option value="O">Pengeluaran</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="form-label">Keterangan</label>
          <div class="input-group col-md-12">
            <textarea name="description" id="description" class="form-control" placeholder="Keterangan" required></textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="form-label">Jenis</label>
          <div class="input-group col-md-12">
            <input type="text" name="tag" id="transactiontag" class="form-control" placeholder="Jenis (Jimpitan, Iuran, Belanja, dll)" required />
          </div>
        </div>
        <div class="form-group">
          <label class="form-label">Nominal</label>
          <div class="input-group col-md-12">
            <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
            <input type="number" name="amount" id="amount" class="form-control" placeholder="Nominal harus berupa angka" required />
          </div>
        </div>
        <div class="form-action">
          <div class="input-group col-md-12">
            <button type="submit" class="btn-primary btn">Simpan</button>
          </div>
        </div>
      </form>
      <?php
    }
    if(@$_GET['page'] == "detail"){
      $financeData = $functions->getFinance($_GET['id']);
      $data = $financeData->data->rows[0];
      ?>
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">
            <small>Detail Kas RT</small>
            <a class="btn btn-primary pull-right btn-back" href="laporan_kas.php"><i class="ion-android-arrow-back"></i></a>
          </h1>
        </div>
      </div>
      <form method="POST" class="form" enctype="multipart/form-data" action="laporan_kas.php">
        <div class="form-group">
          <label class="form-label">Pemasukan / Pengeluaran</label>
          <div class="input-group col-md-12">
            <?php echo ($data->type == "I") ? "Pemasukan" : "Pengeluaran"; ?>
          </div>
        </div>
        <div class="form-group">
          <label class="form-label">Keterangan</label>
          <div class="input-group col-md-12">
            <?php echo $data->description; ?>
          </div>
        </div>
        <div class="form-group">
          <label class="form-label">Jenis</label>
          <div class="input-group col-md-12">
            <?php echo $data->tag; ?>
          </div>
        </div>
        <div class="form-group">
          <label class="form-label">Nominal</label>
          <div class="input-group col-md-12">
            <?php echo number_format($data->amount,0); ?>
          </div>
        </div>
      </form>
      <?php
    }
    ?>
  </div>
  <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
<?php require("../footer.php"); ?>