<?php require("../header.php"); ?>
<?php
$pid = $_COOKIE['peopleid'];
$data = $functions->getUser($pid);
$user = $data[0];
?>
<div id="page-wrapper">
  <div class="container-fluid">
    <div id="main-content">
      <div class="card-content">
        <?php
        if(@$_GET['page']=="browse" || !isset($_GET['page'])){
          $filter = array("limit" => 0, 'order' => 'rid:DESC');
          if($user->role == 'wr') {
            $filter['sender_pid'] = $user->pid;
          }
          $event = $functions->indexReport($filter);
          $data = $event->data->rows;
          ?>
          <div class="row">
            <div class="col-lg-12">
              <h1 class="page-header">
                <small>Laporan Warga</small>
                <a class="btn btn-primary pull-right btn-plus" href="laporan_warga.php?page=add"><i class="ion-plus-circled"></i></a>
              </h1>
            </div>
          </div>
          <table class="table table-bordered data-table">
            <thead>
              <tr>
                <th style="width:45%;">Laporan</th>
                <th style="width:10%;"></th>
              </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data as $row) {
             // $user = $functions->getUser($row->sender_pid);
              $warga = $row->sender;
              $class="";
              if($row->response == ""){
                $class="text-danger";
              }
              ?>
              <tr>
                <td>
                <?php
                $description = (strlen($row->description) > 20 ) ? substr($row->description, 0, 20) . "..." : $row->description ;
                $response = (strlen($row->response) > 20 ) ? substr($row->response, 0, 20) . "..." : $row->response ;
                ?>
                  <span class="card-name-label"><strong><?php echo  $warga->name ?></strong></span>
                  <span class="card-mobile-label"><small>Laporan : <?php echo $description; ?></small></span>
                  <span class="card-mobile-label"><small>Tanggal: <?php echo $functions->getDateTime($row->created); ?></small></span>
                  <span class="card-mobile-label"><small>Tanggapan: <?php echo ($row->response == "" ) ? "" : $response ; ?></small></span>
                  <span class="card-mobile-label"><?php echo ($row->response == "" ) ? "<span class='label label-danger'>Belum Ditanggapi</span>" : "<span class='label label-success'>Sudah Ditanggapi</span>" ; ?></span>
                </td>

                <td style="width:30px;" class="text-right">
                  <?php if($user->role == 'rt'){ ?>
                    <a href="laporan_warga.php?page=option&id=<?php echo $row->rid; ?>" class="list-option"><span class="ion-chevron-down"></span></a>
                  <?php } else { ?>
                    <a href="laporan_warga.php?page=detail&id=<?php echo $row->rid; ?>" class="list-option"><span class="ion-information-circled"></span></a>
                  <?php } ?>
                </td>
              </tr>
              <?php } ?>
            </tbody>

          </table>
          <?php }

          if(@$_GET['page']=="edit" && $user->role == 'rt'){
            if(count($_POST)>0){
                        // $_POST['neighbourhood'] = $_COOKIE['rtid'];
             $update = $functions->updateReport($_GET['id'], $_POST);
             if($update->status){
              redirect("laporan_warga.php");
              // echo "<div class='alert alert-success'>Laporan warga berhasil ditanggapi.</div>";
              // echo "<script type='text/javascript'>window.location.href='laporan_warga.php';</script>";
            }
            else {
              $messages = array();
              if ($update->message == 'Validation Error') {
                  foreach ($update->data->rows as $key => $value) {
                      foreach ($value as $message) {
                          $messages[] = strtolower($message);
                      }
                  }
              } else {
                  $messages[] = strtolower($update->message);
              }
              echo "<div class='alert alert-danger'>Laporan warga gagal ditanggapi: ". implode(', ', $messages). "</div>";
            }
          }

          $report = $functions->getReport($_GET['id']);
          $data=$report->data->rows[0];

          $warga = $data->sender;

          ?>
          <div class="row">
            <div class="col-lg-12">
              <h1 class="page-header">
                <small>Tanggapi Laporan Warga</small>
                <a class="btn btn-primary pull-right btn-back" href="laporan_warga.php"><i class="ion-android-arrow-back"></i></a>
              </h1>
            </div>
          </div>
          <form method="POST" class="form" enctype="multipart/form-data" action="laporan_warga.php?page=edit&id=<?php echo $_GET['id']; ?>">
            <div class="form-group">
              <label class="form-label">Nama Warga</label>
              <div class="input-group col-md-12">
                <?php echo $warga->name ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label">Tanggal / Jam</label>
              <div class="input-group col-md-12">
                <?php echo $functions->getDateTime($data->created) ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label">Isi Laporan</label>
              <div class="input-group col-md-12">
                <?php echo $data->description; ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label">Tanggapan</label>
              <div class="input-group col-md-12">
                <textarea type="text" name="response" id="response" class="form-control" required placeholder="Tanggapan Laporan" autofocus><?php echo $data->response; ?></textarea>
              </div>
            </div>

            <div class="form-action">
              <div class="input-group col-md-12">
                <button type="submit" class="btn-primary btn">Tanggapi</button>
              </div>
            </div>
          </form>
          <?php } ?>
          <?php
          if(@$_GET['page']=="add"){
            if(count($_POST)>0){
              $insert = $functions->createReport($_POST);
              if($insert->status){
                echo "<div class='alert alert-success'>Laporan Anda telah disampaikan ke RT, tunggu tanggapan</div>";
              }
              else {
                $messages = array();
                if ($insert->message == 'Validation Error') {
                    foreach ($insert->data->rows as $key => $value) {
                        foreach ($value as $message) {
                            $messages[] = strtolower($message);
                        }
                    }
                } else {
                    $messages[] = strtolower($insert->message);
                }
                echo "<div class='alert alert-danger'>Laporan warga gagal disimpan: ". implode(', ', $messages). "</div>";
              }
            }
            ?>
            <div class="row">
              <div class="col-lg-12">
                <h1 class="page-header">
                  <small>Lapor Ketua RT</small>
                  <a class="btn btn-primary pull-right btn-back" href="laporan_warga.php?page=browse"><i class="ion-android-arrow-back"></i></a>
                </h1>
              </div>
            </div>
            <form class="form" enctype="multipart/form-data" method="POST" action="laporan_warga.php?page=add">
              <div class="form-group">
                <label class="form-label">Isi Laporan</label>
                <div class="input-group col-md-12">
                  <textarea name="description" id="description" class="form-control" placeholder="Tuliskan Laporan Anda" required></textarea>
                </div>
              </div>
              <div class="form-action">
                <div class="input-group col-md-12">
                  <button type="submit" class="btn-primary btn">Laporkan</button>
                </div>
              </div>
            </form>
            <?php } 
              if(@$_GET['page']=="delete"){
                $report = $functions->deleteReport($_GET['id']);
                redirect("laporan_warga.php");
                // echo "<script type='text/javascript'>window.location.href='laporan_warga.php'</script>";
              }
           
          if(@$_GET['page']=="detail"){
          $report = $functions->getReport($_GET['id']);
          $data=$report->data->rows[0];

          $warga = $data->sender;
          ?>
          <div class="row">
            <div class="col-lg-12">
              <h1 class="page-header">
                <small>Detail Laporan Warga</small>
                <a class="btn btn-primary pull-right btn-back" href="laporan_warga.php?page=browse"><i class="ion-android-arrow-back"></i></a>
              </h1>
            </div>
          </div>
          <form method="POST" class="form" enctype="multipart/form-data" action="lapor_rt.php">
            <div class="form-group">
              <label class="form-label">Nama Warga</label>
              <div class="input-group col-md-12">
                <?php echo $warga->name ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label">Tanggal / Jam</label>
              <div class="input-group col-md-12">
                <?php echo $data->created; ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label">Isi Laporan</label>
              <div class="input-group col-md-12">
                <?php echo $data->description; ?>
              </div>
            </div>
            <div class="form-group">
              <label class="form-label">Tanggapan</label>
              <div class="input-group col-md-12">
                <?php echo $data->response; ?>
              </div>
            </div>
          </form>

          <?php } ?>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->
    <?php require("../footer.php"); ?>