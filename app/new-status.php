<?php
require '../header.php';

$loggedUser = $functions->getUserLogged()[0];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  // $title = $_POST['title'];
  $content = $_POST['content'];

  $resp = $functions->postStatus($content);

  // Error
  if ($resp['code'] == 200) {
    // Redirect to index
    // header('Location: ' . $baseurl);
    redirect($baseurl . '/app/detail_rt.php');
    die();
  }

  //TODO show error
  if ($resp['message'] == 'Validation Error') {
    foreach ($resp['data']['rows'] as $key => $val) {
      foreach ($val as $key => $val) {
        echo "<div class='alert alert-danger'>Status tidak boleh kosong</div>";
      }
    }
  } else {
    echo "<div class='alert alert-danger'>" . $resp['message'] . "</div>";
  }
}

?>

<!-- New Comment form  -->
<!-- <div class="wrapper"> -->
<div id="page-wrapper">  
  <div class="container-fluid">
    <div id="main-content">
      <div class="card-content">
        <div class="row">
          <div class="col-lg-12">
            <h1 class="page-header">
              <small>Kirim Status / Pengumuman</small>
              <a class="btn btn-primary pull-right btn-back" href="<?php echo $baseurl . "/app/detail_rt.php"; ?>"><i class="ion-android-arrow-back"></i></a>
            </h1>
          </div>
        </div>
        <div class="wrapper">
          <div class="main-content">
            <section>
              <ul class="media-list">
                <li class="media">
                  <div class="media-left">
                    <a href="#">
                      <?php
                      $avatar = 'http://i.imgur.com/kfyG0il.png';
                      if($loggedUser->photo) {
                        $avatar = $loggedUser->photo;
                      }
                      ?>
                      <div class="img-container activity"><img class="media-object" data-original="<?php echo $avatar ?>" src="<?php echo $avatar ?>" alt="User Avatar"></div>
                    </a>
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading"><a href="#"><?php echo $loggedUser->name ?></a></h4>
                    <form method="post" class="add-comment">
                      <div class="form-group">
                        <textarea class="form-control" name="content" id="textComment" rows="3" placeholder="Bagikan informasi..."></textarea>
                      </div>
                      <button type="submit" class="btn btn-primary pull-right">Kirim</button>
                    </form>
                  </div>
                </li>
              </ul>
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- </div> -->
<?php include '../footer.php' ?>
