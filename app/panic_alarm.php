<?php require("../header.php"); ?>
<div id="page-wrapper">
  <div class="container-fluid">
    <div id="main-content">
      <div class="card-content">

        <?php
        $data = $functions->getUser($_COOKIE['peopleid']);
        if(count($data) == 0){
          echo "Data tidak ditemukan";
          exit;
        }
        $data=$data[0];
        $date = date('Y-m-d H:i:s');
        $type = array(
          '1' => "Kebakaran",
          '2' => "Pencurian",
          '3' => "Bencana Alam",
          '4' => "Kecelakaan",
        );
          ?>
          <script>
            /*function getLocation() {
              if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
              }
            }

            function showPosition(position) {
              $('input[name=latitude]').val(position.coords.latitude);
              $('input[name=longitude]').val(position.coords.longitude);
            }
            getLocation();*/
          </script>
          <?php
          if (count($_POST) > 0) {
            $input = array(
              'sender_pid' => $_POST['sender_pid'],
              'type' => $_POST['type'],
              'created' => strtotime($_POST['created']),
              'latitude' => $_POST['latitude'],
              'longitude' => $_POST['longitude'],
              'neighbourhood_id' => $_POST['neighbourhood_id'],
            );
            $sendalarm = $functions->insertAlarm($input);
            if($sendalarm->code == 200){
              echo "<div class='alert alert-success'>".$sendalarm->message."</div>";
            }
            else {
              $messages = array();
              if ($sendalarm->message == 'Validation Error') {
                  foreach ($sendalarm->data->rows as $key => $value) {
                      foreach ($value as $message) {
                          $messages[] = strtolower($message);
                      }
                  }
              } else {
                  $messages[] = strtolower($sendalarm->message);
              }
              echo "<div class='alert alert-danger'>". implode(', ', $messages) . "</div>";
            }
          }

          ?>
          <style type="text/css">
            .panik-menu {
              text-align: center;
              font-size: 17px;
              font-weight: bold;
              border-radius: 5px;
            }
            .panik-menu .inner {
              background: #cccccc;
              height: 100px;
              padding-top: 41px;
            }
          </style>

          <?php
          if (@$_GET['page'] == 'lihat') {
          ?>
            <div class="row">
              <div class="col-lg-12">
                <h1 class="page-header">
                  <small>Laporan Tanda Bahaya</small>
                  <a class="btn btn-primary pull-right btn-back" href="panic_alarm.php"><i class="ion-android-arrow-back"></i></a>
                </h1>
              </div>
            </div>
            <?php
            $alerts = $functions->getAlarm($data->neighbourhood->nid)->data->rows;
            ?>
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Waktu</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($alerts as $alert) { ?>
                <tr>
                  <td>
                    <span class="card-name-label"><strong><?php echo $alert->sender->name;?></strong></span>
                    <span class="card-mobile-label"><?php echo $type[$alert->type];?></span>
                    <span class="card-mobile-label"><?php echo date("d M Y H:i:s", strtotime($functions->getDateTime($alert->created)));?></span>
                  </td>
                  <td style="width:10px;">
                    <a class="list-option" href="panic_alarm.php?page=detail&id=<?php echo $alert->aid; ?>"><span class="ion-information-circled"></span></a>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>

            <?php
            }
            else if (@$_GET['page'] == 'detail') {

            $detail = $functions->getAlarmDetail(@$_GET['id'])->data->rows;
            $data=$detail[0];
            //$sender = $functions->getUser($data->sender_pid)[0];
            // echo '<pre>';print_r($detail);echo '</pre>';
            $location = $data->latitude . "," . $data->longitude;
            ?>
            <div class="row">
              <div class="col-lg-12">
                <h1 class="page-header">
                  <small>Detail Laporan Tanda Bahaya</small>
                  <a class="btn btn-primary pull-right btn-back" href="panic_alarm.php?page=lihat"><i class="ion-android-arrow-back"></i></a>
                </h1>
              </div>
            </div>

            <div class="row">
              <div class="col-xs-12">
                <div class="form-group">
                  <label class="form-label">Jenis Bahaya</label>
                  <div class="input-group col-md-12">
                    <?php echo $type[$data->type]; ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="form-label">Pengirim</label>
                  <div class="input-group col-md-12">
                    <?php echo $data->sender->name; ?>
                  </div>
                </div>
                <?php if($_COOKIE['kentongantype']=="rt") { ?>
                <div class="form-group">
                  <label class="form-label">No. HP</label>
                  <div class="input-group col-md-12">
                    <?php echo $data->sender->phone; ?>
                  </div>
                </div>
                <?php } ?>
                <div class="form-group">
                  <label class="form-label">Waktu</label>
                  <div class="input-group col-md-12">
                    <?php echo date("d M Y H:i:s",strtotime($functions->getDateTime($data->created))); ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="form-label">Lokasi</label>
                  <div class="input-group col-md-12">
                    <?php echo ($data->latitude != "" && $data->longitude != "" ) ? '<div id="map" data-lat="'. $data->latitude .'" data-lng="' . $data->longitude . '"></div>' : "Lokasi tidak tersedia"; ?>
                    <div id="address" data-lat="<?php echo $data->latitude; ?>" data-lng="<?php echo $data->longitude; ?>"></div>
                  </div>
                </div>
              </div>
            </div>
            <?php } else { ?>

            <div class="row">
              <div class="col-lg-12">
                <h1 class="page-header">
                  <small>Tanda Bahaya</small>
                </h1>
              </div>
            </div>

            <div class="row">
              <div class="col-xs-6">
                <!-- Maling -->
                <form method="POST" class="form alarm-form"
                action="panic_alarm.php?page=kirim">
                <div class="form-group">
                  <div class="input-group col-xs-6">
                    <input type="hidden" name="sender_pid" value="<?php echo $data->pid; ?>"/>
                    <input type="hidden" name="type" value="2"/>
                    <input type="hidden" name="created" value="<?php echo $date; ?>"/>
                    <input type="hidden" name="latitude"/>
                    <input type="hidden" name="longitude"/>
                    <input type="hidden" name="neighbourhood_id" value="<?php echo $data->neighbourhood->nid; ?>"/>
                    <button type="submit" class="hidden"></button>
                    <button class="btn btn-danger btn-thief btn-alarm" type="button">
                      <span class="alert-image"><i class="ion-ios-unlocked"></i></span> Pencurian
                    </button>
                  </div>
                </div>
              </form>
            </div>

            <div class="col-xs-6">
              <!-- Kebakaran -->
              <form method="POST" class="form alarm-form"
              action="panic_alarm.php?page=kirim">
              <div class="form-group">
                <div class="input-group col-xs-6">
                  <input type="hidden" name="sender_pid" value="<?php echo $data->pid; ?>"/>
                  <input type="hidden" name="type" value="1"/>
                  <input type="hidden" name="created" value="<?php echo $date; ?>"/>
                  <input type="hidden" name="latitude"/>
                  <input type="hidden" name="longitude"/>
                  <input type="hidden" name="neighbourhood_id" value="<?php echo $data->neighbourhood->nid; ?>"/>
                  <button type="submit" class="hidden"></button>
                  <button class="btn btn-danger btn-fire btn-alarm" type="button">
                    <span class="alert-image"><i class="ion-flame"></i></span> Kebakaran
                  </button>
                </div>
              </div>
            </form>
          </div>

          <div class="col-xs-6">
            <!-- Kecelakaan -->
            <form method="POST" class="form alarm-form"
            action="panic_alarm.php?page=kirim">
            <div class="form-group">
              <div class="input-group col-xs-6">
                <input type="hidden" name="sender_pid" value="<?php echo $data->pid; ?>"/>
                <input type="hidden" name="type" value="4"/>
                <input type="hidden" name="created" value="<?php echo $date; ?>"/>
                <input type="hidden" name="latitude"/>
                <input type="hidden" name="longitude"/>
                <input type="hidden" name="neighbourhood_id" value="<?php echo $data->neighbourhood->nid; ?>"/>
                <button type="submit" class="hidden"></button>
                <button class="btn btn-danger btn-accident btn-alarm" type="button">
                  <span class="alert-image"><i class="ion-medkit"></i></span> Kecelakaan
                </button>
              </div>
            </div>
          </form>
        </div>

        <div class="col-xs-6">
          <!-- Bencana Alam -->
          <form method="POST" class="form alarm-form"
          action="panic_alarm.php?page=kirim">
          <div class="form-group">
            <div class="input-group col-xs-6">
              <input type="hidden" name="sender_pid" value="<?php echo $data->pid; ?>"/>
              <input type="hidden" name="type" value="3"/>
              <input type="hidden" name="created" value="<?php echo $date; ?>"/>
              <input type="hidden" name="latitude"/>
              <input type="hidden" name="longitude"/>
              <input type="hidden" name="neighbourhood_id" value="<?php echo $data->neighbourhood->nid; ?>"/>
              <button type="submit" class="hidden"></button>
              <button class="btn btn-danger btn-nature btn-alarm" type="button">
                <span class="alert-image"><i class="ion-earth"></i></span> Bencana Alam
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
    <?php } ?>
  </div>


</div>
</div>
<!-- /.container-fluid -->

</div>

<?php
  if(!isset($_GET['page']) || $_GET['page']=="kirim" ){
    ?>
    <div class="col-xs-12">
      <a class="btn btn-primary" href="panic_alarm.php?page=lihat">Lihat Laporan Darurat</a>
    </div>

    <?php
  }
?>
<!-- /#page-wrapper -->
<?php require("../footer.php"); ?>
