<?php
    if(isset($_GET['page']) && $_GET['page']=="excel"){
        require("../autoload.php");
        $datakk = $functions->getUser($_COOKIE['peopleid']);
        $sendEmail = $functions->peopleExport();

        $dataresult =array (
            'email' => $datakk[0]->email,
            'status' => (isset($sendEmail->status) ? $sendEmail->status : true) ,
            'message' => (isset($sendEmail->message) ? $sendEmail->message : true)
        );
        echo json_encode($dataresult);
        exit;
    }

?>
<?php require("../header.php"); ?>

<?php
    if(count(array_filter($_POST)) > 0){
        unset($_POST['date_']);
    }
    $max_year = date("Y");
    $default_date = date("Y-m-d");
    $familyType = $functions->familyType()->data->rows;                         
?>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div id="main-content">
                <div class="card-content">
                <?php
                if(@$_GET['page']=="browse" || !isset($_GET['page'])){
                    $filter = array();
                    $user = $functions->getUser($_COOKIE["peopleid"])[0];

                    if(@$_COOKIE["kentongantype"]=='wr') $filter['family_id'] = $user->family_id;
                    $data = $functions->indexUser($filter);

                    if(!isset($data)){
                        echo "Data Not Found";
                        exit;
                    }
                ?>
                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                <small>Daftar Warga Anda</small>
                                <a class="btn btn-primary pull-right btn-plus" href="warga.php?page=add"><i class="ion-plus-circled"></i></a>
                                <a class="btn btn-primary pull-right btn-excel" href="warga.php?page=excel"><i class="ion-code-download"></i></a>
                            </h1>
                        </div>
                    </div>
                    <div id="export_excel"></div>

                    <table class="table table-bordered data-table table-warga">
                        <thead>
                        <tr>
                            <th>Nama</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($data as $row) { 
                                $display = array();
                                if($row->phone) {
                                    $display[] = $row->phone;
                                }
                                if ($row->email) {
                                    $display[] = $row->email; 
                                }
                            ?>
                            <tr>
                                <td>
                                    <div class="img-container">
                                        <img data-original="<?php echo ($row->photo != '') ? $apiurl . $row->photo : 'http://i.imgur.com/kfyG0il.png' ?>" alt="" />
                                    </div>
                                    <?php if( strtotime($row->updated) > strtotime($row->created)){
                                        echo "<img src='".$baseurl."/assets/images/favicons/favicon-16x16.png' class='kentongan-sign'>";
                                    } ?>
                                    <span class="card-name-label"><?php echo $row->name ?></span>
                                    <span class="card-phone-label"><?php echo implode(', ', $display); ?></span>
                                </td>
                                <td style="width:30px;" class="text-right">
                                    <a href="warga.php?page=option&id=<?php echo $row->pid; ?>" class="list-option"><span class="ion-chevron-down"></span></a>
                                    <!-- <div class="btn-group">
                                      <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></button>
                                      <ul class="dropdown-menu action-dropdown">
                                        <li><a class='btn btn-success' title="Edit" href="warga.php?page=edit&id=<?php echo $row->pid; ?>"><i class="fa fa-edit"></i></a></li>
                                        <li><a class='btn btn-success' title="Keluarga" href="warga.php?page=keluarga&id=<?php echo $row->pid; ?>"><i class="fa fa-users"></i></a></li>
                                        <li><a class='btn btn-danger confirm' title="Delete" href="warga.php?page=delete&id=<?php echo $row->pid; ?>"><i class="fa fa-trash"></i></a></li>
                                      </ul>
                                    </div> -->
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                <?php }
                elseif (@$_GET['page'] == "add"){
                ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                <small>Undang Warga Bergabung</small>
                                <a class="btn btn-primary pull-right btn-back" href="warga.php"><i class="ion-android-arrow-back"></i></a>
                            </h1>
                        </div>
                    </div>
                    <?php
                    $status = FALSE;
                        if(count($_POST) > 0) {
                            $invite = $functions->inviteUser($_POST);
                            $rows = @$invite->data->rows;
                            $status = TRUE;
                            $error_msg = array();
                            if(isset($rows)){
                                foreach($rows as $result){
                                  $dest = '';
                                  if (!$result->status) {
                                    if (!isset($error_msg[$result->message]) ) $error_msg[$result->message] = array();
                                    if (isset($result->phone)) {
                                      $dest = $result->phone;
                                    } else {
                                      $dest = $result->email;
                                    }
                                    $error_msg[$result->message][] = $dest;
                                  }
                                }
                            }
                        }

                        if (count($_POST)) {
                            if ($_POST['email'] != "" || $_POST['phone'] != "") {
                              if(count($error_msg)){
                                $messages = array();
                                foreach ($error_msg as $key => $value ) {
                                  $messages [] = implode(', ', $value) . ' ('. strtolower($key). ') ';
                                }
                                echo "<div class='alert alert-danger'> Undangan berhasil dikirim kecuali untuk ". implode(', ', $messages). " </div>";
                              } else {
                                echo "<div class='alert alert-success'> Undangan berhasil dikirim</div>";
                                $_POST = array();
                              }
                            }
                            else {
                                echo "<div class='alert alert-danger'> Kolom email atau No. HP harus diisi</div>";
                            }
                        }
                    ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <form method="POST" class="form" action="warga.php?page=add">
                                <?php
                                /* 
                                <div class="form-group">
                                    <label class="form-label">List Email</label>
                                    <div class="input-group col-md-12">
                                        <textarea name="email" id="email" class="form-control" placeholder="email1, email2, email3, dst"><?php echo @$_POST['email']; ?></textarea>
                                    </div>
                                    <span><small>Ajak warga menggunakan kentongan melalui email, pisahkan dengan tanda koma</small></span>
                                </div>
                                */
                                ?>
                                <div class="form-group">
                                    <label class="form-label">List No HP</label>
                                    <div class="input-group col-md-12">
                                        <textarea name="phone" id="phone" class="form-control" placeholder="No HP1, No HP2, No HP3, dst"><?php echo @$_POST['phone']; ?></textarea>
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-contact" type="button" id="open_contact" onclick="openContact();"><i class="ion-person-stalker"></i></button>
                                        </span>
                                    </div>
                                    <span><small>Ajak warga menggunakan kentongan melalui sms, pisahkan dengan tanda koma atau tambahkan melalui kontak.</small></span>
                                </div>

                                <div class="form-action">
                                    <button class="btn btn-primary" type="submit"> Undang </button>
                                </div>

                            </form>
                            <script type="text/javascript">
                                function openContact(){
                                    var contact = Android.openContact();
                                    // document.getElementById("phone").value = ( document.getElementById("phone").value ) + "," + contact;
                                }
                            </script>
                        </div>
                    </div>
                <?php }
                elseif (@$_GET['page'] == "delete"){
                    $delete = $functions->deleteUser($_GET['id']);
                    if( $delete->status == true ){
                        if($_COOKIE['peopleid'] == $_GET['id']){
                            header("location:" . $baseurl."/logout.php");
                        }
                    }
                    header("location:warga.php");
                }
                elseif (@$_GET['page'] == "activate"){
                    $functions->activateUser($_GET['id']);
                    header("location:warga.php");
                }
                
                elseif(@$_GET['page'] == "edit"){
                    if( count($_POST) > 0){
                        $update = $functions->updateUser($_GET['id'], $_POST);
                        if($update->code == 200){
                            echo "<div class='alert alert-success'>".$update->message."</div>";
                        }
                        else {
                            $messages = array();
                            if ($update->message == 'Validation Error') {
                                foreach ($update->data->rows as $key => $value) {
                                    foreach ($value as $message) {
                                        $messages[] = strtolower($message);
                                    }
                                }
                            } else {
                                $messages[] = strtolower($update->message);
                            }
                            echo "<div class='alert alert-danger'>Data warga tidak berhasil diubah: ". implode(', ', $messages)."</div>";
                        }
                    }
                    $data = $functions->getUser($_GET['id']);
                    if(!isset($data)){
                        echo "Data Not Found";
                        exit;
                    }
                    $data=$data[0];
                ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                <small>Perbaharui Data Warga</small>
                                <a class="btn btn-primary pull-right btn-back" href="warga.php"><i class="ion-android-arrow-back"></i></a>
                            </h1>
                        </div>
                    </div>
                    <form method="POST" class="form" enctype="multipart/form-data" action="warga.php?page=edit&id=<?php echo $_GET['id']; ?>">
                        <input type="hidden" name="pid" value="<?php echo $_GET['id']; ?>">
                        
                        <div class="form-group">
                            <div class="input-group col-md-12">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <label for="name">Nama</label>
                                <input value="<?php echo $data->name; ?>" type="text" name="name" id="name" class="form-control" placeholder="Nama" maxlength="20"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-12">
                                <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                <label for="address">Alamat</label>
                                <textarea name="address" id="address" class="form-control" placeholder="Alamat"><?php echo $data->address; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-12 date">
                                <span class="input-group-addon"><i class="fa fa-calendar" data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
                                <label for="birth_date">Tanggal Lahir</label>
                                <input value="<?php echo $data->birth_date; ?>" id="birth_date" name="birth_date" class="form-control" type="date" placeholder="Tanggal Lahir" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-12">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <label for="birth_place">Tempat Lahir</label>
                                <input value="<?php echo $data->birth_place; ?>" type="text" name="birth_place" id="birth_place" class="form-control" placeholder="Tempat Lahir" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-12">
                                <span class="input-group-addon"><i class="fa fa-mars"></i></span>
                                <label for="gender">Jenis Kelamin</label>
                                <select name="gender" class="form-control" id="gender">
                                    <option value="L" <?php echo ($data->gender=="L") ? "selected" : null; ?>>Laki Laki</option>
                                    <option value="P" <?php echo ($data->gender=="P") ? "selected" : null; ?>>Perempuan</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-12">
                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                <label for="phone">Nomor HP</label>
                                <input value="<?php echo $data->phone; ?>" type="text" name="phone" id="phone" class="form-control" placeholder="Nomor HP" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-12">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <label for="email">Email</label>
                                <input value="<?php echo $data->email; ?>" type="text" name="email" id="email" class="form-control" placeholder="Email" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-12">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <label for="education">Tingkat Pendidikan</label>
                                <!--<input value="<?php /*echo $data->education; */?>" type="text" name="education" id="education" class="form-control" placeholder="Tingkat Pendidikan" />-->
                              <select name="education" class="form-control" id="educationlevel">
                                <option value="Tidak Sekolah" <?php echo (@$data->education == 'Tidak Sekolah') ? 'selected' : ''; ?>>Tidak Sekolah</option>
                                <option value="TK" <?php echo (@$data->education == 'TK') ? 'selected' : ''; ?>>TK</option>
                                <option value="SD" <?php echo (@$data->education == 'SD') ? 'selected' : ''; ?>>SD</option>
                                <option value="SMP" <?php echo (@$data->education == 'SMP') ? 'selected' : ''; ?>>SMP</option>
                                <option value="SMA/SMK" <?php echo (@$data->education == 'SMA/SMK') ? 'selected' : ''; ?>>SMA/SMK</option>
                                <option value="D1" <?php echo (@$data->education == 'D1') ? 'selected' : ''; ?>>D1</option>
                                <option value="D2" <?php echo (@$data->education == 'D2') ? 'selected' : ''; ?>>D2</option>
                                <option value="D3" <?php echo (@$data->education == 'D3') ? 'selected' : ''; ?>>D3</option>
                                <option value="S1" <?php echo (@$data->education == 'S1') ? 'selected' : ''; ?>>S1</option>
                                <option value="S2" <?php echo (@$data->education == 'S2') ? 'selected' : ''; ?>>S2</option>
                                <option value="S3" <?php echo (@$data->education == 'S3') ? 'selected' : ''; ?>>S3</option>
                              </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-12">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <label for="role">Hak Akses</label>
                                <div class="row-fluid">
                                    <div class="radio form-control col-md-6">
                                        <input type="radio" name="role" value="rt" <?php echo ($data->role=='rt') ? "checked" : null ?> /> <span>Pengurus RT</span>
                                    </div>
                                    <div class="radio form-control col-md-6">
                                        <input type="radio" name="role" value="wr" <?php echo ($data->role=='wr') ? "checked" : null ?> /> <span>Warga</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-12">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <label for="family_type">Status di Keluarga</label>
                                <select name="family_type" id="family_type" class="form-control">
                                    <?php
                                        foreach($familyType as $family){
                                            $selected = ($family->ftid == $data->family_type) ? "selected" : null;
                                            echo "<option ".$selected." value='".$family->ftid."'>".$family->name."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <?php /*
                        <div class="form-group">
                            <div class="input-group col-md-12">
                                <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                <label for="card_id">No. KTP / NIK</label>
                                <input value="<?php echo $data->card_id; ?>" type="text" name="card_id" id="card_id" class="form-control" placeholder="No. KTP / NIK (optional)" />
                            </div>
                        </div>
                        */ ?>
                        <div class="form-group">
                            <div class="input-group col-md-12">
                                <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                <label for="family_id">Kode Keluarga</label>
                                <input value="<?php echo $data->family_id; ?>" type="text" name="family_id" id="family_id" class="form-control" placeholder="Kode yang sama adalah satu keluarga" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-12">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <label for="#">Foto</label>
                                <label for="photo_img">
                                <input type="file" name="photo_img" id="photo_img" class="form-control" placeholder="Foto"  onclick="choosePhoto();"/>
                                </label>
                                <input type="hidden" name="photo" id="photo" />
                                <div id="files" class="files"><img id="photo_preview" data-original="<?php echo $data->photo; ?>" /></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-12">
                                <button type="submit" class="btn-primary btn">Simpan</button>
                            </div>
                        </div>
                    </form>
                <?php
                }
                elseif(@$_GET['page']=="keluarga"){
                    $datakk = $functions->getUser($_GET['id']);
                    $data = $functions->getFamily($datakk[0]->family_id, $_GET['id'])->data->rows;
                    $family = array("" => "Belum diset");
                    foreach($familyType as $families){
                        $family[$families->ftid] = $families->name;
                    }
                ?>
                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                <small class="pull-left"><?php echo ($datakk[0]->name == "") ? "Silakan isi <a href='biodata_pribadi.php'>Biodata Pribadi</a>" : $datakk[0]->name ; ?></small>
                                <a class="btn btn-primary btn-back btn-inline" href="warga.php"><i class="ion-android-arrow-back"></i></a>
                                <?php 
                                // print_r($datakk[0]);
                                if($datakk[0]->family_type == "KK"){ ?>
                                    <a class="btn btn-primary btn-plus btn-inline" href="warga.php?page=tambahkeluarga&id=<?php echo $datakk[0]->pid; ?>"><i class="ion-plus-circled"></i></a>
                                <?php } ?>
                            </h1>
                        </div>
                    </div>
                    <table class="table table-bordered data-table">
                        <thead>
                        <tr>
                            <th>Nama</th>
                            <th style="width:30px"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($data as $row) { ?>
                            <tr>
                                <td>
                                    <div class="img-container"><img data-original="<?php echo ($row->photo != '') ? $apiurl.$row->photo : 'http://i.imgur.com/kfyG0il.png' ?>" alt="" /></div>
                                    <?php if( strtotime($row->updated) > strtotime($row->created)){
                                        echo "<img src='".$baseurl."/assets/images/favicons/favicon-16x16.png' class='kentongan-sign'>";
                                    } ?>
                                    <span class="card-name-label"><?php echo $row->name ?></span>
                                    <span class="card-phone-label"><?php echo $row->phone ?></span>
                                </td>
                                <td style="width:30px" class="text-right">
                                    <a href="warga.php?page=optioneditkeluarga&id=<?php echo $_GET['id']; ?>&familyid=<?php echo $row->pid; ?>" class="list-option"><span class="ion-chevron-down"></span></a>
                                    <!-- <div class="btn-group">
                                      <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="ion-chevron-down"></span></button>
                                      <ul class="dropdown-menu action-dropdown">
                                        <li><a class="btn btn-success btn-plus" href="warga.php?page=editkeluarga&id=<?php echo $_GET['id'] ?>&familyid=<?php echo $row->peoplefamilyid ?>"><i class="ion-edit"></i></a></li>
                                        <li><a class="btn btn-danger confirm" href="warga.php?page=deletekeluarga&id=<?php echo $_GET['id'] ?>&familyid=<?php echo $row->peoplefamilyid ?>"><i class="ion-trash-a"></i></a></li>
                                      </ul>
                                    </div> -->
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                <?php }

                elseif(@$_GET['page']=="tambahkeluarga"){
                ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                <small>Tambah Data Keluarga</small>
                                <a class="btn btn-primary pull-right btn-back" href="warga.php?page=keluarga&id=<?php echo $_GET['id']; ?>"><i class="ion-android-arrow-back"></i></a>
                            </h1>
                        </div>
                    </div>
                    <?php
                        if(count($_POST) > 0) {
                            unset($_POST['date_']);
                            $datakk = $functions->getUser($_GET['id']);
                            $_POST['neighbourhood_id'] = $datakk[0]->neighbourhood->nid;
                            $_POST['family_id'] = $datakk[0]->family_id;
                            $_POST['role'] = "wr";
                            $insert = $functions->insertPeople($_POST);
                            // echo "<pre>";print_r($insert);echo "</pre>";
                            if($insert->code == 200){
                                echo "<div class='alert alert-success'>".$insert->message."</div>";
                            }
                            else {
                                foreach($insert->data->rows as $e_key => $e_value ){
                                    echo "<div class='alert alert-danger'>".$e_value[0]."</div>";
                                }
                            }
                        }
                    ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form" method="POST" enctype="multipart/form-data" action="warga.php?page=tambahkeluarga&id=<?php echo $_GET['id']; ?>">
                                <input type="hidden" name="password" value="" />
                                <div class="form-group">
                                    <label class="form-label">Nama</label>
                                    <div class="input-group col-md-12">
                                        <input value="<?php echo @$_POST['name']; ?>" type="text" name="name" id="name" class="form-control" placeholder="Nama" maxlength="20"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Email</label>
                                    <div class="input-group col-md-12">
                                        <input value="<?php echo @$_POST['email']; ?>" type="email" name="email" id="email" class="form-control" placeholder="Email" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">No. Telepon</label>
                                    <div class="input-group col-md-12">
                                        <input value="<?php echo @$_POST['phone']; ?>" type="text" name="phone" id="phone" class="form-control" placeholder="No. Telepon" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Tempat Lahir</label>
                                    <div class="input-group col-md-12">
                                        <input value="<?php echo @$_POST['birth_place']; ?>" type="text" name="birth_place" id="birth_place" class="form-control" placeholder="Tempat Lahir" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Tanggal Lahir</label>
                                    <div class="input-group col-md-12">
                                        <input value="<?php echo @$_POST['birth_date']; ?>" type="date" name="birth_date" id="birth_date" class="form-control date" placeholder="Tanggal lahir" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Jenis Kelamin</label>
                                    <div class="input-group col-md-12">
                                        <select name="gender" class="form-control" id="gender">
                                            <option <?php echo @$_POST['gender']=="L" ? "selected" : null ; ?> value="L" >Laki Laki</option>
                                            <option <?php echo @$_POST['gender']=="P" ? "selected" : null ; ?> value="P" >Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="input-group col-md-12">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <label for="family_type">Status di Keluarga</label>
                                    <select name="family_type" id="family_type" class="form-control">
                                        <?php
                                            foreach($familyType as $family){
                                                $selected = ($_POST['family_type'] == $family->ftid) ? "selected" : null ;
                                                echo "<option ".$selected." value='".$family->ftid."'>".$family->name."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="input-group col-md-12">
                                        <label for="education">Tingkat Pendidikan</label>
                                        <!--<input value="<?php /*echo @$_POST['education']; */?>" type="text" name="education" id="education" class="form-control" placeholder="Tingkat Pendidikan" />-->
                                      <select name="education" class="form-control" id="educationlevel">
                                        <option value="Tidak Sekolah" <?php echo (@$_POST['education'] == 'Tidak Sekolah') ? 'selected' : ''; ?>>Tidak Sekolah</option>
                                        <option value="TK" <?php echo (@$_POST['education'] == 'TK') ? 'selected' : ''; ?>>TK</option>
                                        <option value="SD" <?php echo (@$_POST['education'] == 'SD') ? 'selected' : ''; ?>>SD</option>
                                        <option value="SMP" <?php echo (@$_POST['education'] == 'SMP') ? 'selected' : ''; ?>>SMP</option>
                                        <option value="SMA/SMK" <?php echo (@$_POST['education'] == 'SMA/SMK') ? 'selected' : ''; ?>>SMA/SMK</option>
                                        <option value="D1" <?php echo (@$_POST['education'] == 'D1') ? 'selected' : ''; ?>>D1</option>
                                        <option value="D2" <?php echo (@$_POST['education'] == 'D2') ? 'selected' : ''; ?>>D2</option>
                                        <option value="D3" <?php echo (@$_POST['education'] == 'D3') ? 'selected' : ''; ?>>D3</option>
                                        <option value="S1" <?php echo (@$_POST['education'] == 'S1') ? 'selected' : ''; ?>>S1</option>
                                        <option value="S2" <?php echo (@$_POST['education'] == 'S2') ? 'selected' : ''; ?>>S2</option>
                                        <option value="S3" <?php echo (@$_POST['education'] == 'S3') ? 'selected' : ''; ?>>S3</option>
                                      </select>
                                    </div>
                                </div>
                                <?php /*
                                <div class="form-group">
                                    <label class="form-label">No. KTP / NIK</label>
                                    <div class="input-group col-md-12">
                                        <input value="<?php echo @$_POST['card_id']; ?>" type="text" name="card_id" id="card_id" class="form-control" placeholder="No. KTP / NIK" />
                                    </div>
                                </div>
                                */ ?>
                                <div class="form-group">
                                    <div class="input-group col-md-12">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <label for="#">Foto</label>
                                        <label for="photo_img">
                                        <input type="file" name="photo_img" id="photo_img" class="form-control" placeholder="Foto"  onclick="choosePhoto();"/>
                                        </label>
                                        <input type="hidden" name="photo" id="photo" />
                                        <div id="files" class="files"><img id="photo_preview" data-original="<?php echo $data->photo; ?>" /></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group col-md-12">
                                        <button type="submit" class="btn-primary btn">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <?php }

                elseif(@$_GET['page']=="editkeluarga"){
                    if( count($_POST) > 0){
                        $update = $functions->updateUser($_GET['familyid'],$_POST);
                        if($update->code == 200){
                            echo "<div class='alert alert-success'>".$update->message."</div>";
                        }
                        else {
                            echo "<div class='alert alert-danger'>".$update->message."</div>";
                        }
                    }
                    if(!isset($_GET['familyid'])){
                        echo "Invalid parameter..!";
                        exit;
                    }
                    $data = $functions->getUser($_GET['familyid']);
                    if(!isset($data)){
                        echo "Data Not Found";
                        exit;
                    }
                    $data=$data[0];
                ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                <small>Sesuaikan Data Keluarga</small>
                                <a class="btn btn-primary pull-right btn-back" href="warga.php?page=keluarga&id=<?php echo $_GET['id'] ?>"><i class="ion-android-arrow-back"></i></a>
                            </h1>
                        </div>
                    </div>
                    <form class="form" method="POST" enctype="multipart/form-data" action="warga.php?page=editkeluarga&id=<?php echo $_GET['id']; ?>&familyid=<?php echo $_GET['familyid']; ?>">
                        <input type="hidden" name="password" value="" />
                        <div class="form-group">
                            <label class="form-label">Nama</label>
                            <div class="input-group col-md-12">
                                <input value="<?php echo @$data->name; ?>" type="text" name="name" id="name" class="form-control" placeholder="Nama" maxlength="20"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Email</label>
                            <div class="input-group col-md-12">
                                <input value="<?php echo @$data->email; ?>" type="email" name="email" id="email" class="form-control" placeholder="Email" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">No. Telepon</label>
                            <div class="input-group col-md-12">
                                <input value="<?php echo @$data->phone; ?>" type="text" name="phone" id="phone" class="form-control" placeholder="No. Telepon" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Tempat Lahir</label>
                            <div class="input-group col-md-12">
                                <input value="<?php echo @$data->birth_place; ?>" type="text" name="birth_place" id="birth_place" class="form-control" placeholder="Tempat Lahir" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Tanggal Lahir</label>
                            <div class="input-group col-md-12">
                                <input value="<?php echo @$data->birth_date; ?>" type="date" name="birth_date" id="birth_date" class="form-control date" placeholder="Tanggal lahir" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Jenis Kelamin</label>
                            <div class="input-group col-md-12">
                                <select name="gender" class="form-control" id="gender">
                                    <option <?php echo @$data->gender=="L" ? "selected" : null ; ?> value="L" >Laki Laki</option>
                                    <option <?php echo @$data->gender=="P" ? "selected" : null ; ?> value="P" >Perempuan</option>
                                </select>
                            </div>
                        </div>
                        <div class="input-group col-md-12">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <label for="family_type">Status di Keluarga</label>
                            <select name="family_type" id="family_type" class="form-control">
                                <?php
                                    foreach($familyType as $family){
                                        $selected = ($data->family_type == $family->ftid) ? "selected" : null ;
                                        echo "<option ".$selected." value='".$family->ftid."'>".$family->name."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-12">
                                <label for="education">Tingkat Pendidikan</label>
                                <!--<input value="<?php /*echo @$data->education; */?>" type="text" name="education" id="education" class="form-control" placeholder="Tingkat Pendidikan" />-->
                              <select name="education" class="form-control" id="educationlevel">
                                <option value="Tidak Sekolah" <?php echo (@$data->education == 'Tidak Sekolah') ? 'selected' : ''; ?>>Tidak Sekolah</option>
                                <option value="TK" <?php echo (@$data->education == 'TK') ? 'selected' : ''; ?>>TK</option>
                                <option value="SD" <?php echo (@$data->education == 'SD') ? 'selected' : ''; ?>>SD</option>
                                <option value="SMP" <?php echo (@$data->education == 'SMP') ? 'selected' : ''; ?>>SMP</option>
                                <option value="SMA/SMK" <?php echo (@$data->education == 'SMA/SMK') ? 'selected' : ''; ?>>SMA/SMK</option>
                                <option value="D1" <?php echo (@$data->education == 'D1') ? 'selected' : ''; ?>>D1</option>
                                <option value="D2" <?php echo (@$data->education == 'D2') ? 'selected' : ''; ?>>D2</option>
                                <option value="D3" <?php echo (@$data->education == 'D3') ? 'selected' : ''; ?>>D3</option>
                                <option value="S1" <?php echo (@$data->education == 'S1') ? 'selected' : ''; ?>>S1</option>
                                <option value="S2" <?php echo (@$data->education == 'S2') ? 'selected' : ''; ?>>S2</option>
                                <option value="S3" <?php echo (@$data->education == 'S3') ? 'selected' : ''; ?>>S3</option>
                              </select>
                            </div>
                        </div>
                        <?php /*
                        <div class="form-group">
                            <label class="form-label">No. KTP / NIK</label>
                            <div class="input-group col-md-12">
                                <input value="<?php echo @$data->card_id; ?>" type="text" name="card_id" id="card_id" class="form-control" placeholder="No. KTP / NIK" />
                            </div>
                        </div>
                        */ ?>
                        <div class="form-group">
                            <div class="input-group col-md-12">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <label for="#">Foto</label>
                                <label for="photo_img">
                                <input type="file" name="photo_img" id="photo_img" class="form-control" placeholder="Foto"  onclick="choosePhoto();"/>
                                </label>
                                <input type="hidden" name="photo" id="photo" />
                                <div id="files" class="files"><img id="photo_preview" data-original="<?php echo $data->photo; ?>" /></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-12">
                                <button type="submit" class="btn-primary btn">Simpan</button>
                            </div>
                        </div>
                    </form>
                <?php
                    }
                    elseif(@$_GET['page']=="deletekeluarga"){
                        $delete = $functions->deleteUser($_GET['familyid']);
                        header("location:warga.php?page=keluarga&id=".$_GET['id']);
                    }
                ?>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->
<script type="text/javascript">
    var maxYear = "<?php date('Y'); ?>";
    var defaultDate = "<?php echo @$data->birth_date; ?>";
</script>
<script type="text/javascript">
    
    function showAndroidToast(toast) {
        Android.showToast(toast);
    }
    function setFilePath(file) {
        document.getElementById('photo').value = file;
        Android.showToast(file);
    }
    function setFileUri(uri) {
        document.getElementById('photo_preview').src = uri;
        Android.showToast(uri);
    }
    function choosePhoto() {
        var file = Android.choosePhoto();
        // window.alert("file = " + file);
    }
</script>
<?php require("../footer.php"); ?>
