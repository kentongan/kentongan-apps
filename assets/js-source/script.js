$(function(){
  function getCookieData( name ) {
    var patrn = new RegExp( "^" + name + "=(.*?);" ),
      patr2 = new RegExp( " " + name + "=(.*?);" );
    if ( match = (document.cookie.match(patrn) || document.cookie.match(patr2)) )
      return match[1];
    return false;
  }

  if (window.location.pathname != "/login.php" && getCookieData("peopleid") == "") {
    //window.location.replace("/login.php");
  }
	$('.table').dataTable({
		"bSort" : false,
		"autoWidth": false,
		oLanguage: {
		    sSearch: ''
		}
	});
	
	$('.table').on( 'draw.dt', function () {
	     controls();
	} );

	$("button.btn-alarm").click(function(){
		var submit = $(this).parent().find("[type=submit]");
		bootbox.confirm("Apakah Anda yakin mengirimkan tanda bahaya ?",function(e){
			if( e == 1 ){
				submit.click();
			}
		});
		return false;
	});

	$("li.disabled").hide();

	$(".search form select").change(function(){
		$("[type=submit]").click();
	});

	$(document).ready(function(){			
		$('textarea').elastic();
		$('textarea').trigger('update');
	});	

	$("img").lazyload();
	
  	controls();
	function controls(){
		$("a.confirm").click(function(){
			var target = $(this).attr("href");
			bootbox.confirm("Apakah Anda yakin ?",function(e){
				if( e == 1 ){
					window.location.href = target ;
				}
			});
			return false;
		});

		$("li.disabled").hide();

		$("#main-content table tr").click(function(){
			var target = $(this).children("td").children("a").attr("href");
			if( target == "undefined" || target == "" || target == undefined){
				return false;
			}
			window.location.href=$(this).children("td").children("a").attr("href");
			return false;
		});
		$("#main-content table tr td a").click(function(){
			window.location.href=$(this).attr("href");
			return false;
		});

		$("textarea").elastic();
		$("textarea").trigger("update");

		$("img").lazyload();

	}

	$("#photo_img").change(function(){
        readImage( this );
    });

    $("#validation_doc_img").change(function(){
        readImage( this );
    });

    function readImage(input) {
	    if ( input.files && input.files[0] ) {
	        var FR= new FileReader();
	        FR.onload = function(e) {
	        	// console.log('e.target.result', e.target.result);
	             $("#files img").attr("src", e.target.result);
	             $("#photo").val( e.target.result );
	        };       
	        FR.readAsDataURL( input.files[0] );
	    }
	}

	//get city
    $("#province_id").change(function(){
        var target = "../system/functions.php?type=getcity";
        var datapost = {
          'provinceid' : $(this).val()
        }
        $.post(target, datapost, function( e ){
          $("#city_id").html( e );
        });
    });

    $("#city_id").change(function(){
      var target = "../system/functions.php?type=getdistrict";
      var datapost = {
        'cityid' : $(this).val(),
        'provinceid' : $("#province_id").val()
      }
      $.post(target, datapost, function( e ){
        $("#district_id").html( e );
      });
    });
});