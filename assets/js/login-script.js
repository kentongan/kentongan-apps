var owl_item_width = $( window ).width();
var owl_width = 0;
var owl_transform = 0;

$(document).ready(function(){
  $("img.logo-log").show();
});

(function($) {
  "use strict"; // Start of use strict
  $(document).ready(function() {
    init_owl();
    if( owl_width == 0){
      owl_width = $(".owl-stage").width();
      owl_transform = $(".owl-stage").css("transform");
    }
    if( kentonganversion > 0 ){
        if(navigator.userAgent.toLowerCase().indexOf("android") > -1){
          $('.popup-info').modal('show');
          $('.popup-info').modal({backdrop: 'static', keyboard: false});
        }
    }

    $('a').click(function(){
      $('body').removeClass('login-option');
    });
    $('.account-option, label.back, .test_rt, a.update').click(function(){
      $('body').addClass('login-option');
    });
    containerHeight();
  });

  $(window).load(function(){
    containerHeight();
   
  });
  $(window).resize(function(){
    containerHeight();
   
  });
  function containerHeight() {
    var winHeight = $(window).height();
    $('.form__container, .owl-carousel .owl-stage-outer, .owl-carousel .owl-stage').height(winHeight);
  }
  function owlitemWidth() {
    var winWidth = $(window).width();
    $('.owl-item').width(winWidth);
  }

})(jQuery); // End of use strict

function init_owl(){
  // $("#owl-slide").owlCarousel({
  //   loop:true,
  //   margin:0,
  //   nav:true,
  //   autoplay: true,
  //   autoplayTimeout: 6000,
  //   dragBeforeAnimFinish:true,
  //   items:1
  // });
}

function getCookieData( name ) {
  var patrn = new RegExp( "^" + name + "=(.*?);" ),
  patr2 = new RegExp( " " + name + "=(.*?);" );
  if ( match = (document.cookie.match(patrn) || document.cookie.match(patr2)) )
    return match[1];
  return false;
}

function hideall(){
  // alert("action");
  // $(".form__option").hide();
  $(".owl-item").hide();
  $(".form__option ul.list-inline").hide();
  $(".form__option").css("visibility","hidden");
  $("div.alert").hide();
  $(".form__login").hide();
  $(".form__signup").hide();
  $(".form__summon").hide();
  $(".reset__form").hide();
  $(".btn-option").hide();
  $(".form-reg1").hide();
  $(".form-reg2").hide();
  $(".city_tmp").after('<select name="city" id="city" class="city form-control" placeholder="Kabupaten" required><option value="">Pilih Kabupaten / Kota</option></select>');
  $(".district_tmp").after('<select name="district" id="district" class="district form-control" placeholder="Kecamatan" required><option value="">Pilih Kecamatan</option></select>');
  $(".city_tmp").remove();
  $(".district_tmp").remove();
  $(".province").val("");
  $(".city").val("");
  $(".district").val("");
  init_owl();
}

// $(function(){
  $(".city").change(function(){
    if($(this).val() == "0"){
      var this_el = $(this);
      this_el.parent().after("<input type='text' maxlength='50' class='form-control city_tmp' name='city_tmp' id='city_tmp' placeholder='Ketik Nama Kabupaten / Kota Anda' />");

      var district_el = this_el.parent().parent().find(".district");
      district_el.parent().after("<input type='text' maxlength='50' class='form-control district_tmp' name='district_tmp' id='district_tmp' placeholder='Ketik Nama Kecamatan Anda' />");
      this_el.parent().hide();
      district_el.parent().hide();
    }
  });

  $(".district").change(function(){
    if($(this).val() == "0"){
      $(this).parent().after("<input type='text' maxlength='50' class='form-control district_tmp' name='district_tmp' id='district_tmp' placeholder='Ketik Nama Kecamatan Anda' />");
      $(this).parent().hide();
    }
  });

  $(".back").click(function(){
    hideall();
    $(".form__option").css("visibility","visible");
    $(".form__option ul.list-inline").show();
    $('#owl-slide').trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
    $('#owl-slide').find('.owl-stage-outer').children().unwrap();
    init_owl();
  });
  $(".guide_page").click(function(){
    hideall();
    window.location.href="cara-pemakaian-mobile";
  });

  $(".reset_form").click(function(){
    hideall();
    $(".reset__form").show();
  });

  $(".register_rt").click(function(){
    hideall();
    $(".form__signup").show();
  });
  $(".summon_rt").click(function(){
    hideall();
    $(".form__summon").show();
  });
  $(".btn_option").click(function(){
    hideall();
    $(".btn-option").show();
  });

  $(".to_login").click(function(){
    hideall();
    $("html").css("height", "100%");
    $(".form__login").show();
  });

  $(".to_step1").click(function(){

    hideall();
    $('#owl-slide').trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
    $('#owl-slide').find('.owl-stage-outer').children().unwrap();
    init_owl();
    
    $(".form__signup").show();
    $(".form-reg1").show();
    $(".form-reg2").hide();
    $(".btn-option").hide();
  });

  $(".test_rt").click(function(){
    hideall();
    window.location.href="test_rt.php";
  });

  if( login_error == true ){
    $(".to_login").click();
    $("div.alert").show();
    $("body").removeClass("login-option");
  }
  if( test_rt_error == true ) {
    $(".to_login").click();
    $("div.alert").show();
    $("body").removeClass("login-option");
  }

  if( reset_error == true ){
    $(".reset_form").click();
    $("div.alert").show();
    $("body").removeClass("login-option");
  }

  if( reset_success == true ){
    $(".reset_form").click();
    $("div.alert").show();
    $("body").removeClass("login-option");
  }

  $("form.summon").submit(function(){
    var target = $(this).attr("action");
    var datapost = $(this).serialize();
    $.post(target,datapost,function( e ){
      // console.log(e);
      // var html_text = '<div class="alert-info alert kentongan">' + e.response + '</div>';
      $("#register_status").html( e.response );
      $("#register_status").addClass("alert");
      $("#register_status").addClass("kentongan");
      if(e.status){

        $("#register_status").removeClass("alert-danger");
        $("#register_status").addClass("alert-success");
        // $("#register_status").removeClass( "alert-danger" ).addClass( "alert-success" );
        // Reset Form Summon
        $(".summon #name").val('');
        $(".summon #rt_name").val('');
        $(".summon #rt_phone").val('');
        $(".summon #province").val('');
        $(".summon #city").val('');
        resetErrors();
      }
      else {
        $("#register_status").removeClass("alert-success");
        $("#register_status").addClass("alert-danger");
        formDisplayError( e, "summon" );
      }
      $("#register_status").show(function(){
        setTimeout("$('#register_status').hide();",3000);
      });
    },"json");
    return false;
  });

  if (window.location.pathname == "/login.php" && getCookieData("accesstoken") != "") {
    window.location.replace("blocker.php");
  }

  function resetErrors() {
    $(".register input, .register select").removeClass("input-error");
    $(".summon input, .summon select").removeClass("input-error");
    $(".form-error-message").remove();

  }
  function formDisplayError(data,form) {
    resetErrors();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
    //var messages = '';
    $.each(data.message, function(i, v) {
      $.each(v, function(k, m) {
        if (m !== "") {
          // $input_el = $('.register input[name="' + i + '"], .register select[name="' + i + '"]');
          $input_el = $('.' + form + ' input[name="' + i + '"], .' + form + ' select[name="' + i + '"]');
          //messages += '<li>' + m + '</li>';
          if (!$input_el.hasClass('input-error')) {
            $input_el.addClass('input-error');
            $input_el.after("<span class=\"form-error-message\">"+ m+ "</span>")
          }
        }
      });
    });
    //$(".form__signup").prepend('<div class="alert-info alert kentongan"><ul>'+ messages+'</ul></div>');
  }

  $(".flipper__checkbox").change(function(){
    if(this.checked) {
      $(".form__signup").show();
      $("html").css("height", "auto");
      $(".form__login").hide();
    } else {
      $("html").css("height", "100%");
      $(".form__login").show();
      $(".form__signup").hide();
      $(".form-reg1").show();
      $(".form-reg2").hide();
      $(".flipper__checkbox2").attr('checked', false);
    }
  });

  $("#next_step").click(function(){
    if( $(".register #name").val() == "" || $("#card_id").val() == "" || $("#family_id").val() == "" || $("#email").val() == "" || $("#phone").val() == "" ){
      $("span.form-error-message").remove();
      $(".register #name,#card_id,#family_id,#email,#phone").removeClass("input-error");
      $(".register #name,#card_id,#family_id,#email,#phone").each(function(){
        if( $(this).val() == "" && !$(this).hasClass("input-error") ){
          var name = $(this).attr("placeholder");
          $(this).addClass("input-error");
          $(this).after("<span class='form-error-message'>Kolom " + name + " harus diisi</span>");
        }
      });
    }
    else {
      var url = "ajax/register_rt_validation.php";
      var datapost ={
        'name' : $("#register_form #name").val(),
        'card_id' : $("#register_form #card_id").val(),
        'family_id' : $("#register_form #family_id").val(),
        'email' : $("#register_form #email").val(),
        'phone' : $("#register_form #phone").val(),
        'step' : 'step1',
      }
      $.post(url,datapost,function( e ){
        resetErrors();
        if( e.status == true){
          resetErrors();
          $(".form-reg1").hide(function(){
            $(".form-reg2").show();
          });
        }
        else {
          formDisplayError( e, "register" );
        }
      },"json");
    }
  });
  $("#goto_step1").click(function(){
    $(".form-reg2").hide("slow",function(){
      $(".form-reg1").show("slow");
    });
  });

  var winHeight =  $(window).height();
  $('.form__container').height(winHeight);

  //add validation for register rt
  var formValid = false;

  $("#register_form").submit(function(){
    var target = "ajax/register_rt_validation.php";
    var datapost = $(this).serialize();
    $.post(target,datapost,function( e ){
      if(e.status == true){
        $("#submitregister").attr("disabled",true);
        $("#submitregister").html("Silakan Tunggu ...");
        var loginurl = "register_rt.php";
        $.post(loginurl,datapost,function( reg ){
          console.log( reg );
          if( reg.status == true ){
            $("#login-content").html("<div style='text-align:center;' class='success_register'>Pendaftaran Berhasil, silakan cek sms</div>");
            setTimeout("window.location.href='login.php';",3000);
          }
          else {
            var step = reg.step;
            var errorhtml = "";
            if(step==""){
              $("#login-content").html("<div style='text-align:center;' class='success_register'>Pendaftaran Berhasil, silakan cek sms</div>");
              setTimeout("window.location.href='login.php';",3000);
            }
            else if(step == "step1"){
              // $("#goto_step1").click();
              $.each( reg.error,function(i,v){
                if(errorhtml == ""){
                  errorhtml = v;
                }
                else {
                  errorhtml = errorhtml + "<br />" + v;
                }
              });
              $("#register_status").addClass("alert-danger");
              $("#register_status").html(errorhtml);
              $("#submitregister").removeAttr("disabled");
              $("#submitregister").html("Daftar Sekarang");
            }
            else {
              $.each( reg.error,function(i,v){
                errorhtml = errorhtml + "<br />" + v;
              });
              $("#register_status").addClass("alert-danger");
              $("#register_status").html(errorhtml);
              $("#submitregister").removeAttr("disabled");
              $("#submitregister").html("Daftar Sekarang");
            }
          }
          $("#register_status").slideDown("medium",function(){
            setTimeout("$('#register_status').slideUp('medium');",3000);
          });
        },"json");
      }
      else {
        formDisplayError( e, "register" );
      }
    },"json");

    return false;

  });

  $(".province").change(function(){
    var elem = $(this);
    var target = "system/functions.php?type=getcity";
    var datapost = {
      'provinceid' : elem.val()
    }
    $.post(target,datapost,function( e ){
      elem.parent().parent().find(".city").html( e );
    });
  });

  $(".city").change(function(){
    var elem = $(this);
    var target = "system/functions.php?type=getdistrict";
    var datapost = {
      'cityid' : elem.val(),
      'provinceid' : elem.parent().parent().find(".province").val()
    }
    $.post(target,datapost,function( e ){
      elem.parent().parent().find(".district").html( e );
    });
  });
// });