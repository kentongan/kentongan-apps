<?php
	require("system/functions.php");
	$usertoken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwaWQiOjE2NSwiZW1haWwiOiJhZG1pbkBrZW50b25nYW4ub3JnIn0.bpYOUL8ort01Wt-zGM5zvroLAdIk2K3vfd6QyPb-6qg";
	$getAppBuild = $functions->appBuildVersion($usertoken);
	$appBuild = $getAppBuild->data->rows[0];

	// echo "<pre>";print_r($appBuild);echo "</pre>";
	$result = array(
		'code' => 200,
		'status' => true,
		'message' => "Application Build Version",
		'data' => array(
			'total' => 1,
			'count' => 1,
			'rows' => array(
				(array) $appBuild
			)
		),
	);

	echo json_encode($result);
?>