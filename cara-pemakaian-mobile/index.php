<!DOCTYPE html>
<html lang="en">

<head>
  <link rel="manifest" href="/manifest.json">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1  user-scalable=no">

  <!-- components -->
  <link href="css/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="components/owl.carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
  <link href="components/owl.carousel/owl-carousel/owl.theme.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link rel="stylesheet" href="css/style.min.css" type="text/css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body>

  <section>
    <?php if( !isset($_COOKIE["accesstoken"]) || is_null($_COOKIE["accesstoken"]) ){ ?>
    <nav class="text-right nav-close bg-primary">
     <!--  <button class="close" onclick="window.location.href='../index.php';"><span>&times;</span></button> -->
    </nav>
    <?php } ?>
    <div class="container">
      <div id="owl-slide" class="owl-carousel owl-theme">

        <div class="item">
          <img src="img/logo-lingkaran.png" alt="Kentongan" class="img-responsive">
          <div class="caption-slide">
            <h2 class="text-primary text-center">Selamat Datang &#128522;</h2>
            <p>
              Selamat datang di Kentongan. Apabila Anda membutuhkan bantuan atau memiliki pertanyaan tentang Kentongan, dapat menghubungi :
            </p>
            <table class="table">
              <tr>
                <td style="width:40px;"><img src="img/phone-24.png" /></td>
                <td>+62 274 4547428</td>
              </tr>
              <tr>
                <td><img src="img/whatsapp-24.png" /></td>
                <td>+6285643448875</td>
              </tr>
              <tr>
                <td><img src="img/mail-24.png" /></td>
                <td><a href="mailto:lapor@kentongan.org">lapor@kentongan.org</a></td>
              </tr>
              <tr>
                <td><img src="img/wordpress-24.png" /></td>
                <td><a href="http://kentongan.org">http://kentongan.org</a></td>
              </tr>
              <tr>
                <td><img src="img/facebook-24.png" /></td>
                <td><a href="https://facebook.com/kentongan">https://facebook.com/kentongan</a></td>
              </tr>
            </table>
          </div>
        </div>

        <div class="item">
          <img src="img/biodatapribadi.png" alt="Kentongan" class="img-responsive">
          <div class="caption-slide">
            <h2 class="text-primary"><span class="num">1</span>. Lengkapi Biodata Pribadi</h2>
            <p>Sebelum menggunakan aplikasi kentongan, Anda perlu melengkapi biodata pribadi. Data ini sangat diperlukan oleh Ketua RT untuk administrasi dan pemantauan warga. Data yang dimasukkan akan divalidasi oleh Ketua RT.</p>
          </div>
        </div>
        <div class="item">
          <img src="img/warga.png" alt="Kentongan" class="img-responsive">
          <div class="caption-slide">
            <h2 class="text-primary"><span class="num">2</span>. Undang Warga</h2>
            <p>Kentongan dapat berfungsi sempurna apabila digunakan oleh ketua RT dan warga. Agar warga dapat ikut serta untuk menggunakan kentongan. Ketua RT harus mengundang satu per satu warganya melalui email atau SMS.</p>
          </div>
        </div>
        <div class="item">
          <img src="img/kasrt.png" alt="Kentongan" class="img-responsive">
          <div class="caption-slide">
            <h2 class="text-primary"><span class="num">3</span>. Catat Kas RT</h2>
            <p>Catatlah setiap ada pemasukan atau pengeluaran kas RT. Sehingga pelaporan kas RT dapat lebih transparan, karena seluruh warga dapat melihat laporan kas RT secara <span style="font-style:italic">realtime</span>. </p>
          </div>
        </div>
        <div class="item">
          <img src="img/laporanwarga.png" alt="Kentongan" class="img-responsive">
          <div class="caption-slide">
            <h2 class="text-primary"><span class="num">4</span>. Lapor RT</h2>
            <p>Sampaikan laporan, saran, keluhan Anda langsung ke ketua RT. Seperti halnya tamu yang bermalam, kerusakan sarana RT, atau saran untuk kemajuan RT. Laporan ini akan ditanggapi oleh Ketua RT.</p>
          </div>
        </div>
        <div class="item">
          <img src="img/infokegiatan.png" alt="Kentongan" class="img-responsive">
          <div class="caption-slide">
            <h2 class="text-primary"><span class="num">5</span>. Info Kegiatan RT</h2>
            <p>Apabila RT akan mengadakan suatu kegiatan misalnya kerja bakti, lomba-lomba, kumpulan RT atau kegiatan lain. Tulislah di info kegiatan RT dan seluruh warga akan langsung mendapatkan pemberitahuan.</p>
          </div>
        </div>
        <div class="item">
          <img src="img/tandabahaya.png" alt="Kentongan" class="img-responsive">
          <div class="caption-slide">
            <h2 class="text-primary"><span class="num">6</span>. Laporkan Tanda Bahaya</h2>
            <p>Laporkan keadaan darurat Anda dengan tanda bahaya. Misalnya Anda melihat pencurian, kebakaran, kecelakaan atau bencana alam. Sehingga seluruh warga dapat bersiap dan ikut serta membantu keadaan darurat Anda.</p>
          </div>
        </div>

      </div>
    </div>
  </section>


 
  <!-- jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

  <!-- Bootstrap Core JavaScript -->
  <script src="css/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
  <script src="components/owl.carousel/owl-carousel/owl.carousel.min.js"></script>
  <script src="js/script.js"></script>
  <script type="text/javascript">
  WebFontConfig = {
    google: { families: [ 'Lato:400,300,300italic,400italic,700,900:latin', 'Open+Sans:400,700:latin' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })(); </script>



</body>

</html>
