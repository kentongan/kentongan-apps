</div>

<script type="text/javascript" src="<?php echo $baseurl; ?>/assets/js/jquery.js?<?php echo $rand_static ?>"></script>
<script type="text/javascript" src="<?php echo $baseurl; ?>/assets/js/bootstrap.min.js?<?php echo $rand_static ?>" ></script>
<script src="<?php echo $baseurl; ?>/assets/js/jquery.date-dropdowns.min.js?<?php echo $rand_static ?>"></script>
<script src="<?php echo $baseurl; ?>/assets/js/jquery.dataTables.min.js?<?php echo $rand_static ?>"></script>
<script src="<?php echo $baseurl; ?>/assets/js/dataTables.bootstrap.min.js?<?php echo $rand_static ?>"></script>
<script src="<?php echo $baseurl; ?>/assets/js/jquery.jscroll.min.js?<?php echo $rand_static ?>"></script>

<script src="<?php echo $baseurl; ?>/assets/js/app-script.min.js?<?php echo $rand_static ?>"></script>
<?php if($_SERVER['PHP_SELF'] == $app_directory . "/index.php") { ?>
  <!-- <script id="index_gmap" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwydKMEo7-u0CU7PRoAPi9lA71uDtiqDU&signed_in=false"></script> -->
  <script type="text/javascript">
    $("#index-container").html("<img src='<?php echo $baseurl;?>/assets/images/gears.gif' class='gear' />");
    $("#index-container").load("<?php echo $baseurl . '/app/detail_rt.php'; ?>");
    if(typeof google == "undefined"){
      var imported = document.createElement('script');
      imported.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBwydKMEo7-u0CU7PRoAPi9lA71uDtiqDU&signed_in=false';
      document.body.appendChild(imported);
    }
  </script>
<?php } ?>
<?php if($_SERVER['PHP_SELF'] == $app_directory . "/app/detail_rt.php" || $_SERVER['PHP_SELF'] == "/index.php") { ?>
<script id="rt_gmap" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwydKMEo7-u0CU7PRoAPi9lA71uDtiqDU&signed_in=false"></script>
<script type="text/javascript">
  eachMap();
  function eachMap(){
    $(".map.unloaded").each(function(index,elem){
      var elemId = $(elem).attr("id");
      initMap(elemId);
    });
  }
  function initMap( element ) {
    var splitted = element.split("_");
    var idnum = splitted[1];
    var dataLat = eval( $("#" + element).attr("data-lat") );
    var dataLng = eval( $("#" + element).attr("data-lng") );
    var myLatLng = {lat : dataLat, lng: dataLng };

    var map = new google.maps.Map(document.getElementById( element ), {
      zoom: 18,
      center: myLatLng,
      draggable: false,
      scaleControl: false,
    });

    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      title: 'Lokasi Pengirim'
    });

    $("#" + element).removeClass("unloaded");
    $.getJSON("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + dataLat + "," + dataLng + "&sensor=true",function( e ){
      var address = "";
      if( e.status != "ZERO_RESULTS" ){
        address = e.results[0].formatted_address;
      }
      else{
        address = "Alamat tidak ditemukan";
      }
      $("#address_" + idnum).html( "Lokasi : " + address );
    });
    
  }
</script>
<?php } ?>
<script type="text/javascript">
$(function(){
  $('.media-list').jscroll({
    nextSelector: '.next',
    callback: function(){
      eachMap();
    }
  });

  $('.hidden-comment').hide();
  $('#show-comments').click(function(e){
    e.preventDefault();
    $('#show-comments').hide();
    $('.hidden-comment').show();
  })
});
</script>


<?php if($_SERVER['PHP_SELF'] == $app_directory . "/app/biodata_pribadi.php" || $_SERVER['PHP_SELF'] == "/app/warga.php" || $_SERVER['PHP_SELF'] == "/app/keluarga.php"){ ?>
<script type="text/javascript">

    $(function() {
        $("#birth_date").dateDropdowns({
            submitFormat: "dd/mm/yyyy",
            maxYear : maxYear,
            defaultDate: defaultDate,
            submitFormat: "yyyy-mm-dd",
            defaultDateFormat: "yyyy-mm-dd",
            required: true,
            daySuffixes: false
        });

        // if(navigator.userAgent.toLowerCase().indexOf("android") < 1){
        //   $("#open_contact").hide();
        // }

        // $('.date-dropdowns').parent().parent().find("label").css("color","inherit");

        $('.date-dropdowns').append('<label for="day" class="select-box"></label>');
        $('select.day').appendTo( $('label[for="day"]') );
        $('select.day').attr('id','day');

        $('.date-dropdowns').append('<label for="month" class="select-box"></label>');
        $('select.month').appendTo( $('label[for="month"]') );
        $('select.month').attr('id','month');

        $('.date-dropdowns').append('<label for="year" class="select-box"></label>');
        $('select.year').appendTo( $('label[for="year"]') );
        $('select.year').attr('id','year');
    });
</script>
<?php } ?>

<?php if( $_SERVER['PHP_SELF'] == $app_directory . "/app/info_kegiatan.php" ){ ?>
<script type="text/javascript">
$(function() {
  $("#select-startdate").dateDropdowns({
    minYear : minYear,
    maxYear : maxYear,
    defaultDate: $("#select-startdate").attr("data-value"),
    submitFieldName: 'select-startdate',
    submitFormat: "yyyy-mm-dd",
    required: true,
    daySuffixes: false,
  });
  $("#select-enddate").dateDropdowns({
    minYear : minYear,
    maxYear : maxYear,
    defaultDate: $("#select-enddate").attr("data-value"),
    submitFieldName: 'select-enddate',
    submitFormat: "yyyy-mm-dd",
    required: true,
    daySuffixes: false,
  });


  $('.date-dropdowns select').each(function(){
    $(this).wrap('<label for="' + $(this).attr("class") + '" class="select-box"></label>');
    $(this).attr("id",$(this).attr("class"));
  });

});
</script>
<?php } ?>
<?php if( $_SERVER['PHP_SELF'] == $app_directory . "/app/panic_alarm.php" ){ ?>
  <script type="text/javascript">
    function androidSetPosition(lat, lng) {
    	if(lat == 0 && lng == 0){
    		if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(showPosition);
        }
        lat = position.coords.latitude;
        lng = position.coords.longitude;
    	}
      $('input[name=latitude]').val(lat);
      $('input[name=longitude]').val(lng);
    }

    function getLocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
      }
    }

    function showPosition(position) {
      $('input[name=latitude]').val(position.coords.latitude);
      $('input[name=longitude]').val(position.coords.longitude);
    }
    getLocation();
  </script>
  <?php if(isset($_GET) && $_GET['page']=="detail") { ?>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwydKMEo7-u0CU7PRoAPi9lA71uDtiqDU&signed_in=false&callback=initMap"></script>
  <script type="text/javascript">
    function initMap() {
      var dataLat = eval( $("#map").attr("data-lat") );
      var dataLng = eval( $("#map").attr("data-lng") );
      var myLatLng = {lat : dataLat, lng: dataLng };

      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 18,
        center: myLatLng,
        draggable: false,
        scaleControl: false,
      });

      var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Lokasi Pengirim'
      });
      $.getJSON("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + dataLat + "," + dataLng + "&sensor=true",function( e ){
        var address = "";
        if( e.status != "ZERO_RESULTS" ){
          address = e.results[0].formatted_address;
        }
        else{
          address = "Alamat tidak ditemukan";
        }
        $("#address").html( "Alamat : " + address );
      });
    }
  </script>
  <?php } ?>
<?php } ?>
<?php if( $_SERVER['PHP_SELF'] == $app_directory . "/app/laporan_kas.php" ){ ?>
<style>
  div.dataTables_wrapper div.dataTables_filter label {
    margin-top: 18px;
  }
</style>
<script type='text/javascript'>
  $(function(){
    $(".detail_kas").click(function(){
      window.location.href="laporan_kas.php?page=detail&id=" + $(this).attr("id");
    });
  });
</script>
<?php } ?>

<?php if( $_SERVER['PHP_SELF'] == $app_directory . "/app/warga.php" ){ ?>
<script type='text/javascript'>
  $(function(){
    if(navigator.userAgent.toLowerCase().indexOf("iphone") > -1){
      $("#open_contact").removeAttr("onclick");
      $("#open_contact").wrap("<a href='warga.php?page=contact'>","</a>");
    }
  });
</script>
<?php } ?>
<script>
  var update = false;
  <?php
    $appVersion = $functions->appBuildVersion();
    $androidVersion = $appVersion->data->rows[0]->android;
    if (!isset($_SERVER['HTTP_KENTONGANVERSION'])) {
  ?>
      if(navigator.userAgent.toLowerCase().indexOf("android") > -1){
        //popup info
        $('.popup-info').modal('show');
        $('.popup-info').modal({backdrop: 'static', keyboard: false});
      }
  <?php
    } else {
      $kentongan_version = $_SERVER['HTTP_KENTONGANVERSION'];
      $temp = explode('-', $kentongan_version);
      $os = strtolower($temp[0]);
      $version = $temp[1];
      if ($androidVersion > $version) {
        echo "update = true;";
      }
    }
  ?>
  if(update){
    if(navigator.userAgent.toLowerCase().indexOf("android") > -1){
      //popup info
      $('.popup-info').modal('show');
      $('.popup-info').modal({backdrop: 'static', keyboard: false});
    }
  }
  function solvingTextarea(){
    $("textarea").trigger('update');
  }
</script>
</body>

</html>
