var gulp = require('gulp'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    cssMin = require('gulp-minify-css'),
    concat = require('gulp-concat'),
    order = require('gulp-order'),
    sourcemaps = require('gulp-sourcemaps')
    assetsPath = 'assets';

var paths = {
	js : {
		core: {
			name: 'script-core.min.js',
			src: [
				assetsPath+ '/js-source/jquery.js',
			]
		},
		login : {
			name: 'login-script.min.js',
			src: [
				assetsPath+ '/js-source/bootstrap.min.js',
				assetsPath+ '/js-source/owl.carousel.js',
				assetsPath+ '/js-source/login-script.js',
			]
		},
		app : {
			name: 'app-script.min.js',
			src: [
				/*assetsPath+ '/js-source/bootstraps.min.js',
				*/
				//assetsPath+ '/js-source/jquery.dataTables.min.js',
				//assetsPath+ '/js-source/dataTables.bootstrap.min.js',
				assetsPath+ '/js-source/bootbox.min.js',
				//assetsPath+ '/js-source/jquery.date-dropdowns.min.js',
				assetsPath+ '/js-source/jquery.elastic.source.min.js',
				assetsPath+ '/js-source/jquery.lazyload.min.js',
				assetsPath+ '/js-source/script.min.js',
			]
		},
		dst: assetsPath + '/js',
	},
	css : {
		login : {
			name: 'login-style.min.css',
			src: [
				assetsPath+ '/css-source/bootstrap.min.css',
				assetsPath+ '/css-source/login.min.css',
				assetsPath+ '/css-source/owl.carousel.css',
				assetsPath+ '/css-source/login.min.css',
				//assetsPath+ '/ionicon/css/ionicons.min.css',
			]
		},
		app: {
			name: 'app-style.min.css',
			src: [
				assetsPath+ '/css-source/bootstrap.min.css',
				/*
				//assetsPath+ '/ionicon/css/ionicon.min.css',
				*/
				assetsPath+ '/css-source/sb-admin.css',
				assetsPath+ '/css-source/jquery.dataTables.min.css',
				assetsPath+ '/css-source/dataTables.botstrap.min.css',
				assetsPath+ '/css-source/jquery.timepicker.min.css',
				assetsPath+ '/css-source/style.min.css',
			]
		},
		dst: assetsPath + '/css',
	}
};

gulp.task('minify-js-core', function ()
{
    return gulp.src(paths.js.core.src)
       .pipe(order(paths.js.core.src))
       .pipe(concat(paths.js.core.name))
       .pipe(uglify())
       .pipe(gulp.dest(paths.js.dst));
});

gulp.task('minify-js-app', function ()
{
    return gulp.src(paths.js.app.src)
       .pipe(order(paths.js.app.src))
       .pipe(concat(paths.js.app.name))
       .pipe(uglify())
       .pipe(gulp.dest(paths.js.dst));
});

gulp.task('minify-js-login', function ()
{
    return gulp.src(paths.js.login.src)
	  //  .pipe(sourcemaps.init())
       .pipe(order(paths.js.login.src))
       .pipe(concat(paths.js.login.name))
       //.pipe(gulp.dest(paths.js.dst))
       //.pipe(rename('uglify.js'))
       .pipe(uglify())
       //.pipe(sourcemaps.write('./'))
       .pipe(gulp.dest(paths.js.dst));
});

gulp.task('minify-css-login', function (){
    return gulp.src(paths.css.login.src)
               .pipe(concat(paths.css.login.name))
               //.pipe(cssMin())
               .pipe(gulp.dest(paths.css.dst));
});

gulp.task('minify-css-app', function (){
    return gulp.src(paths.css.app.src)
               .pipe(concat(paths.css.app.name))
               .pipe(cssMin())
               .pipe(gulp.dest(paths.css.dst));
});

gulp.task('watch', ['minify-js-login', 'minify-js-core', 'minify-js-app', 'minify-css-login', 'minify-css-app'], function ()
{
    gulp.watch(paths.js.login.src, ['minify-js-login']);
    gulp.watch(paths.js.core.src, ['minify-js-core']);
    gulp.watch(paths.js.core.src, ['minify-js-app']);
    gulp.watch(paths.css.login.src, ['minify-css-login']);
    gulp.watch(paths.css.login.src, ['minify-css-app']);
});


gulp.task('default', ['minify-js-login', 'minify-js-app']);
