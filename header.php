<?php
    foreach ($_SERVER as $key => $value) {
        if( strpos($key, "HTTP_") !== FALSE ){
          @header($key . ": " . $value);
        }
    }
    require_once("autoload.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">
    <title>Kentongan</title>
    <link href="<?php echo $baseurl; ?>/assets/css/bootstrap.min.css?<?php echo $rand_static ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo $baseurl; ?>/assets/css/sb-admin.css?<?php echo $rand_static ?>" rel="stylesheet">
    <link href="<?php echo $baseurl; ?>/assets/ionicon/css/ionicons.min.css?<?php echo $rand_static ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo $baseurl; ?>/assets/css/jquery.dataTables.min.css?<?php echo $rand_static ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo $baseurl; ?>/assets/css/dataTables.bootstrap.min.css?<?php echo $rand_static ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo $baseurl; ?>/assets/css/jquery.timepicker.min.css?<?php echo $rand_static ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo $baseurl; ?>/assets/css/style.min.css?<?php echo $rand_static ?>" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js?<?php echo $rand_static ?>"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js?<?php echo $rand_static ?>"></script>
    <![endif]-->
    <script type="text/javascript">
        var offset = new Date().getTimezoneOffset() / (-60);
        var exdays = 365;
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+d.toUTCString();
        document.cookie="timezone=" + offset + "; " + expires + "; path=/app";
    </script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-66382480-5', 'auto');
      ga('send', 'pageview');

    </script>
</head>

<body>
    <?php
        // echo "<pre>";print_r($_SERVER);echo "</pre>";
    // Get ID
    $wrapperclass = "";
    $self = $_SERVER['PHP_SELF'];
    $path = explode("/",$self);
    $file = isset($path[2]) ? $path[2] : $path[1];
    if(substr($file,-4) == ".php"){
        $wrapperclass = substr($file,0,strlen($file) - 4);
    }
    ?>
    <div id="wrapper" class="<?php echo $wrapperclass; ?>">
   
    <?php
        if(isset($_COOKIE['demoregister']) ){
            if($_SERVER['PHP_SELF'] != $app_directory . "/index.php" ){
                echo "<div class='alert alert-warning' style='text-align:center;margin:5px 0px;margin-top:-10px; border-radius:0px;'>Ini hanya akun demo / uji coba. <br />Data yang dimasukkan akan diabaikan.<div style='margin-top:10px;'><a href='".$baseurl."/logout.php' class='logout-demo'>Keluar dari mode demo</a></div></div>"; 
            }
        }
        else {
            // Check Validation doc RT
            $data = $functions->getRT($_COOKIE['rtid'])->data->rows;
            $data = $data[0];
            if( is_null($data->approval_photo) || $data->approval_photo == "" ){
             echo "<div class='novalidate'>
                <p>Silakan upload foto cap RT Anda untuk validasi dan menghindari pemblokiran.</p>
                <div class='validate-button'><a class='validate' href='".$baseurl."/app/detail_rt.php?page=edit'>Verifikasi RT</a></div>
             </div>";

             echo "<style>
                #page-wrapper{
                    margin-top : 130px;
                }
             </style>";
            }
            
            // Check Validation Biodata Pribadi
            $dataUser = $functions->getUserLogged();
            $usersLogged = $dataUser[0];
            if( $usersLogged->name == "" ){
                if( $wrapperclass != "biodata_pribadi"){
                    redirect( $baseurl . "/app/biodata_pribadi.php");
                }
            }

            $dataWarga = $functions->indexUser( array() );
            if(count($dataWarga) == 1){
               echo "<div class='no-people'>
                    <p>Anda belum memiliki warga. Undanglah beberapa warga bergabung di Kentongan.</p>
                    <div class='invite-button'><a class='invite' href='".$baseurl."/app/warga.php?page=add'>Undang Warga</a></div>
                 </div>"; 
            }

        }
    ?>

