<?php
  require_once("system/functions.php");
  $provinces = $functions->getProvinces();
  $rand_static = "ktg" . rand(1000,9999);
  $appVersion = $functions->appBuildVersion();
  $androidVersion = $appVersion->data->rows[0]->android;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Kentongan</title>
    <style>
      html,body{
        background-color : #fe5722;
      }
    </style>
    <link href="assets/css/bootstrap.min.css?<?php echo $rand_static; ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo $baseurl; ?>/assets/css/login.min.css?<?php echo $rand_static; ?>" rel="stylesheet" type="text/css" />
    
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-66382480-5', 'auto');
      ga('send', 'pageview');

    </script>
  </head>
  <body class="login-option">
    <!-- <p style="color: #000000;">test</p> -->
    <div class="container page-wrapper" id="login-content">
      <div class="form__container" >
        <input id="flag" type="hidden" value="no" name="flag">
        <div id="register_status" class="kentongan" style="display:none;text-align:center;"></div>
        <?php if (isset($_GET['message']) && $_GET['message']=='login_error') {?>
            <div class="alert-info alert kentongan">
                Email / No. HP atau password salah.
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
        <?php } ?>
        <?php if (isset($_GET['message']) && $_GET['message']=='test_rt_error') {?>
            <div class="alert-info alert kentongan">
                Maaf, test RT gagal dilakukan, cobalah lain waktu.
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
        <?php } ?>
        <?php if (isset($_GET['message']) && $_GET['message']=='reset_error') {?>
            <div class="alert-info alert kentongan">
                <?php echo $_GET['msgcontent']; ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
        <?php } ?>
        <?php if (isset($_GET['message']) && $_GET['message']=='reset_success') {?>
            <div class="alert-info alert kentongan">
                <?php echo $_GET['msgcontent']; ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
        <?php } ?>
        <!-- Login -->
        <div class="form__login">
          <h1></h1>
          <h2 class="tagline">Tingkatkan keamanan warga dan lingkungan RT Anda</h2>
          <form id="login_form" action="do_login.php" method="post" novalidate>
            <input id="username" name="username" type="text" class="form-control" placeholder="Email / No. HP" required>
            <input id="password" name="password" type="password" class="form-control" placeholder="Password" required>
            <label for="flipper__checkbox" class="reset_form form__link text-right"><em>Lupa Password</em></label>

            <div class="text-center">
              <button type="submit" class="btn btn-login">Login</button>
              <label for="flipper__checkbox" class="to_step1 form__link"><em>Mendaftar</em></label>
            </div>
          </form>
        </div>

        <!-- Reset Password -->
        <div class="reset__form" style="display:none;">
          <h1></h1>
          <form id="reset_form" action="reset_password.php" method="post" novalidate>
            <input id="reset_username" name="reset_username" type="text" class="form-control" placeholder="Email / No. HP" value="<?php echo @$_POST['reset_username'] ?>" required>
            <div class="text-center">
              <button type="submit" class="btn btn-login">Kirimi Saya Password</button>
              <label for="flipper__checkbox" class="to_login form__link"><em>Kembali ke Login</em></label>
            </div>
          </form>
        </div>

        <!-- Summon RT -->
        <div class="form__summon" style="display:none;">
          <h2>Sarankan ke Ketua RT</h2>
          <form class="summon" action="summon_rt.php" method="post" novalidate>
            <input id="name" name="name" type="text" class="form-control" placeholder="Nama Anda" maxlength="20" required>
            <input id="rt_name" name="rt_name" type="text" class="form-control" placeholder="Nama Ketua RT Anda" required>
            <input id="rt_phone" name="rt_phone" type="text" class="form-control" placeholder="No HP Ketua RT Anda" required>
            <label for="province" class="select-wrapp">
              <select name="province" id="province" class="province form-control" placeholder="Provinsi" required>
                <option value="">Pilih Provinsi</option>
                <?php foreach ($provinces as $key => $value){ ?>
                  <option value="<?php echo $value->pvid; ?>"><?php echo $value->name; ?></option>
                <?php } ?>
              </select>
            </label>
            <label for="regency" class="select-wrapp">
              <select name="regency" id="city" class="city form-control" placeholder="Kabupaten" required>
                <option value="">Pilih Kabupaten / Kota</option>
              </select>
            </label>
            <div class="text-center">
              <button type="submit" class="btn btn-login">Undang</button>
              <label class="to_login form__link back" type="button"><em>Kembali</em></label>
            </div>
          </form>
        </div>

        <div class="btn-option text-center" style="display: none">
          <img src="<?php echo $baseurl; ?>/assets/images/logo-login.png" alt="kentongan" class="img-responsive logo-log">
          <a href="#" class="test_rt form__link btn btn-login"><img src="<?php echo $baseurl; ?>/assets/images/icon/icon-trial.png" alt="kentongan" class="img-responsive">Coba Sebagai Ketua RT</a>
          <a href="#" class="summon_rt form__link btn btn-login"><img src="<?php echo $baseurl; ?>/assets/images/icon/icon-newsletter.png" alt="kentongan" class="img-responsive">Undang Ketua RT</a>
          <a href="#" class="to_step1 form__link account-option-end btn btn-login"><img src="<?php echo $baseurl; ?>/assets/images/icon/icon-people.png" alt="kentongan" class="img-responsive">Sudah Siap Mendaftar</a>
          <label class="option_menu form__link back" type="button"><em>Kembali</em></label>
        </div>
        <!-- Register -->
        <div class="form__signup" style="display:none;">
          <h2>Pendaftaran Kentongan</h2>
          <p class="text-center">Daftarkan diri dan lingkungan Anda di Kentongan, undang warga dan pengurus RT Anda. Atau coba demo aplikasi.</p>
          <div class="row">
            <div class="col-xs-6 half-button">
              <a href="#" class="test_rt form__link btn btn-login btn-clear">Demo</a>
            </div>
            <div class="col-xs-6 half-button">
              <a href="#" class="summon_rt form__link btn btn-login btn-clear">Sarankan</a>
            </div>
          </div>
          <hr />
          <form class="register" id="register_form" novalidate action="register_rt.php" method="post">
            <div class="reg-wrapper">
              <div class="form-reg1" style="display: none;">
                <input type="text" maxlength="50" name="name" id="name" class="form-control" placeholder="Nama Anda" maxlength="20" required>
                <input type="text" maxlength="15" name="phone" id="phone" class="form-control" placeholder="No. HP" required>
                <div class="row">
                  <div class="col-xs-6">
                    <input type="number" maxlength="5" name="rt" id="rt" class="form-control" placeholder="Nomor RT" required>
                  </div>
                  <div class="col-xs-6">  
                    <input type="number" maxlength="5" name="rw" id="rw" class="form-control" placeholder="Nomor RW" required>
                  </div>
                </div>
                <label for="province" class="select-wrapp">
                <select name="province" id="province" class="province form-control" placeholder="Provinsi" required>
                  <option value="">Pilih Provinsi</option>
                  <?php foreach ($provinces as $key => $value): ?>
                    <option value="<?php echo $value->pvid; ?>"><?php echo $value->name; ?></option>
                  <?php endforeach ?>
                </select>
                </label>
                <label for="city" class="select-wrapp">
                <select name="city" id="city" class="city form-control" placeholder="Kabupaten" required>
                  <option value="">Pilih Kabupaten / Kota</option>
                </select>
                </label>
                <label for="district" class="select-wrapp">
                <select name="district" id="district" class="district form-control" placeholder="Kecamatan" required>
                  <option value="">Pilih Kecamatan</option>
                </select>
                </label>
                <input type="text" maxlength="50" name="village" id="village" class="form-control" placeholder="Tulis Nama Desa" required>
                <input type="hidden" maxlength="50" name="country" id="country" class="form-control" placeholder="Tulis Nama Negara" required value="Indonesia">
                <div class="text-center">
                    <button type="submit" class="btn btn-login" id="submitregister">Daftar Sekarang</button>
                    <label for="flipper__checkbox" class="to_login form__link">Kembali ke Login</label>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- modal popup -->
    <div class="modal fade popup-info" tabindex="-1" role="dialog" aria-labelledby="popup-info" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-body">
            <p>Telah hadir versi Kentongan yang lebih baru. Untuk kinerja dan fitur yang lebih optimal, silakan update Kentongan Anda ke versi terbaru.</p>
            <a href="https://play.google.com/store/apps/details?id=com.skyshi.Kentongan" target="_blank" class="btn btn-success pull-right update">Update</a>
          </div>
        </div>
      </div>
    </div>
    
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700?<?php echo $rand_static; ?>' rel='stylesheet' type='text/css'>
    <link href="<?php echo $baseurl; ?>/assets/css/owl.carousel.min.css?<?php echo $rand_static; ?>" rel="stylesheet">
    <link href="<?php echo $baseurl; ?>/assets/ionicon/css/ionicons.min.css?<?php echo $rand_static; ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo $baseurl; ?>/assets/css/bootstrap.min.css?<?php echo $rand_static; ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo $baseurl; ?>/assets/css/login.min.css?<?php echo $rand_static; ?>" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" src="assets/js/jquery.js?<?php echo $rand_static; ?>"></script>
   
    <script type='text/javascript'>
      var kentonganversion = 0;
      var update = false;
      <?php
        if ( isset($_SERVER['HTTP_KENTONGANVERSION']) ) {
          $kentongan_version = $_SERVER['HTTP_KENTONGANVERSION'];
          $temp = explode('-', $kentongan_version);
          $os = strtolower($temp[0]);
          $version = $temp[1];
          if ($androidVersion > $version) {
            echo "update = true;";
          }
          echo "kentonganversion = $version;";
        }
      ?>
    </script>
    <script type="text/javascript">
      var login_error = false;
      var test_rt_error = false;
      var reset_error = false;
      var reset_success = false;
      <?php if (isset($_GET['message']) && $_GET['message']=='login_error') { ?>
          login_error = true;
      <?php } ?>
      <?php if (isset($_GET['message']) && $_GET['message']=='test_rt_error') {?>
            test_rt_error = true;
      <?php } ?>
      <?php if (isset($_GET['message']) && $_GET['message']=='reset_error') {?>
          reset_error = true;
      <?php } ?>
      <?php if (isset($_GET['message']) && $_GET['message']=='reset_success') {?>
          reset_success = true;
      <?php } ?>  
    </script>
    <script type="text/javascript">
      $("#register_status").hide();
      <?php if (isset($_GET['message'])) { ?>
        setTimeout("$('.alert-info').fadeOut()",3000);
      <?php } ?>
    </script>
    <script type="text/javascript" src="assets/js/login-script.min.js?<?php echo $rand_static; ?>"></script>
  </body>
</html>
