<?php 
	require_once("system/functions.php");
	$result = array( "step" => "","status" => false );
	$result['error']->empty_post[0] = "Empty Post";

	if(count($_POST) > 0){
		unset($_POST['flipper__checkbox']);
		$neighbourhood=array(
			'rt' => $_POST['rt'],
			'rw' => $_POST['rw'],
			'village' => $_POST['village'],
			'district_id' => @$_POST['district'],
			'city_id' => @$_POST['city'],
			'province_id' => $_POST['province'],
			'country' => $_POST['country'],
			'district_tmp' => @$_POST['district_tmp'],
			'city_tmp' => @$_POST['city_tmp'],
			// 'validation_doc' => $_POST['validation_doc'],
		);
		if(isset($_POST['district_tmp'])){
			$neighbourhood['city_id'] = 0;
		}
		if(isset($_POST['city_tmp'])){
			$neighbourhood['district_id'] = 0;
		}
		$insert_rt = $functions->insertRT($neighbourhood);

		if(@$insert_rt->code == 200){
			$nid = $insert_rt->data->rows[0]->nid;
			if( substr($_POST['phone'],0,1) == "0" ){
	          $_POST['phone'] = "+62" . substr($_POST['phone'],1);
	        }
			$peoples = array(
				'name' => $_POST['name'],
				// 'card_id' => $_POST['card_id'],
				// 'family_id' => $_POST['family_id'],
				// 'email' => $_POST['email'],
				'phone' => $_POST['phone'],
				'role' => 'rt',
				'neighbourhood_id' => $nid,
			);
			$insert_people = $functions->insertPeople($peoples);
			if( @$insert_people->status){
				$result['status'] = true;
			}
			else {
				$result['status'] = false;
				$result['step'] = "step1";
				$result['error'] = @$insert_people->data->rows;
			}
		}
		else {
			$result['step'] = "step2";
			$result['error'] = @$insert_rt->data->rows;
			if(count($result['error'])==0){
				$result['error'][0] = "RT Anda sudah pernah didaftarkan";
			}
		}
	}
	echo json_encode($result);
?>