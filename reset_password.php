<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Kentongan</title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,700' rel='stylesheet' type='text/css'>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/login.css" rel="stylesheet" type="text/css" />
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <div class="blocker">
      <h1>Kentongan</h1>
    </div>
  </body>
</html>
<?php 
	require_once("system/functions.php");

	$result = array( "status" => false );
	if(count($_POST) > 0){
		$username = $_POST['reset_username'];
		$reset = $functions->resetPassword($username);
		// echo "<pre>";print_r($reset);echo "</pre>";
		if($reset->status == true){
			echo "<script type='text/javascript'>window.location.href='login.php?message=reset_error&msgcontent=".$reset->message."';</script>";
		}
		else {
			echo "<script type='text/javascript'>window.location.href='login.php?message=reset_success&msgcontent=".$reset->message."';</script>";
		}
	}
	
	echo "<script type='text/javascript'>window.location.href='login.php';</script>";
?>