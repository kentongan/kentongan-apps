<?php
	require_once("system/functions.php");
	$message = array();

	$result = array(
		'status' => FALSE,
		'message' => $message,
		'response' => ""
	);
if(count($_POST) > 0){
	$fields = array(
		'name' => array(
			'label' => "Nama", 
			'type' => "text",
			'maxlength' => 50,
			'required' => true
		),
		'rt_name' => array(
			'label' => "Nama Ketua RT", 
			'type' => "text",
			'maxlength' => 50,
			'required' => true
		),
		'rt_phone' => array(
			'label' => "No. HP Ketua RT", 
			'type' => "number",
			'maxlength' => 15,
			'required' => true
		),
		'province' => array(
			'label' => "Provinsi", 
			'type' => "number",
			'maxlength' => 11,
			'required' => true
		),
	);
	// Start Validation
	$status = true;
	foreach($fields as $_key => $_value){
		$type = ( isset($_value['type']) ) ? $_value['type'] : null;
		$maxlength = ( isset($_value['maxlength']) ) ? intval($_value['maxlength']) : null;
		if(@$_value['required'] == true){
			if (@$_POST[$_key] == "" || !isset($_POST[$_key])) {
				$result['status'] = FALSE;
				$message[$_key][] = $_value['label'] . ' tidak boleh kosong';
			}
		}
		if(@$type == "text"){
			if(!preg_match('/[a-zA-Z\s]+/',@$_POST[$_key])) {
				$result['status'] = FALSE;
				$message[$_key][] = $_value['label'] . " harus berupa alfabet";
			}
		}
		if(@$type == "email"){
			if (!filter_var(@$_POST[$_key], FILTER_VALIDATE_EMAIL)) {
				$result['status'] = FALSE;
				$message[$_key][] = $_value['label'] . " email tidak valid";
			}
		}
		if(@$type == "number"){
			if (!is_numeric(@$_POST[$_key])) {
				$result['status'] = FALSE;
				$message[$_key][] = $_value['label'] . ' harus berupa angka';
			}
		}
		// Checking Maxlength
		if(@$maxlength != null){
			if (strlen(@$_POST[$_key]) > $maxlength) {
				$result['status'] = FALSE;
				$message[$_key][] = $_value['label'] . ' hanya boleh berisi ' . $maxlength . ' karakter';
			}
		}
	}

	$result['message'] = $message;

	if($status){
		$_POST['rt_phone'] = str_replace(" ","",$_POST['rt_phone']);
		$_POST['rt_phone'] = str_replace("-","",$_POST['rt_phone']);
		if(substr($_POST['rt_phone'],0,1) == "0"){
            $_POST['rt_phone'] = "+62" . substr($_POST['rt_phone'],1);
        }
		$data = array(
			'name' => $_POST['name'],
			'rt_name' => $_POST['rt_name'],
			'rt_phone' => $_POST['rt_phone'],
			'province' => $_POST['province'],
			'regency' => @$_POST['regency']
		);
		$insert = $functions->insertInvitor($data);
		$result['status'] = $insert->status;
		$result['response'] = $insert->message;
	}
}
echo json_encode($result);
// echo "<pre>";print_r($result);echo "</pre>";
?>