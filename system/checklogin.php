<?php
	if(!isset($_COOKIE["accesstoken"]) || is_null($_COOKIE["accesstoken"]) || $_COOKIE["accesstoken"] == "" ){
	    header("location:".$baseurl."/logout.php");
	    exit;
	}
// Retrieve Cookie
 require_once("functions.php");
 $datacookie = $functions->getUserLogged();
 if(count($datacookie) > 0){
 	if($datacookie[0]->pid != ""){
	 	if(@$_COOKIE['peopleid'] != $datacookie[0]->pid || @$_COOKIE['kentongantype'] != $datacookie[0]->role || @$_COOKIE['rtid'] != $datacookie[0]->neighbourhood->nid){
		    setcookie("peopleid",$datacookie[0]->pid,time()+3600*24);
		    setcookie("rtid",$datacookie[0]->neighbourhood->nid,time()+3600*24);
		    setcookie("kentongantype",$datacookie[0]->role,time()+3600*24);
		    redirect($_SERVER['REQUEST_URI']);
		    exit();
	 	}
	}
	else {
		redirect($baseurl."/logout.php");
	}
 }
 else {
 	redirect($baseurl."/logout.php");
 }
?>