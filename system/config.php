<?php

$protocol = "https://";
$appstate = "live"; // "dev" or "live"

// API URL
global $apiurl;
if($appstate == "live"){
	if(! isset($_COOKIE['demoregister']) ){
		$apiurl = "https://api.kentongan.org/";
	}
	else {
		$apiurl = "http://demo.api.kentongan.org/";
	}
}
else {
	$protocol = "http://";
	if(! isset($_COOKIE['demoregister']) ){
		$apiurl = "http://dev.api.kentongan.org/";
	}
	else {
		$apiurl = "http://demo.api.kentongan.org/";
	}
}

$app_directory = "/kentongan-apps";
$baseurl = $protocol . $_SERVER['SERVER_NAME'] . $app_directory ;
$script_version = "13.01";
$rand_static = substr(md5($script_version), 5, 6);
?>
