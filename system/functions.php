<?php
require("config.php");
class Functions{
	// Month Year functions
	public function getDays() {
		return [
			'Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'
		];
	}


	function getMonths(){
		$months=array(
	      "01" => "Januari",
	      "02"=> "Februari",
	      "03"=> "Maret",
	      "04"=> "April",
	      "05"=> "Mei",
	      "06"=> "Juni",
	      "07"=> "Juli",
	      "08"=> "Agustus",
	      "09"=> "September",
	      "10" => "Oktober",
	      "11" => "November",
	      "12" => "Desember",
	    );
	    return $months;
	}

	function getYears(){
		$year_s = date("Y") - 5;
		$year_e = date("Y");
		for($i=$year_s;$i<=$year_e;$i++){
			$years[$i] = $i;
		}
	    return $years;
	}

	// Locations Functions
	function getProvinces() {
		$path = "location/province";
		$data = array();
		$method = "GET";
		$dataresult = $this->callApi($path,$data,$method);
		return json_decode($dataresult)->data->rows;
	}

	function getDateTime($datetime){
		$timestamp = strtotime("+".($_COOKIE['timezone'] - 7)." hours",strtotime($datetime));
        $created=date("d-m-Y H:i:s",$timestamp);
        return $created;
	}

	function getCities($provinceId) {
		if (!$provinceId) {
			return FALSE;
		}
		$path = "location/province/".$provinceId."/city";
		$data = array();
		$method = "GET";
		$dataresult = $this->callApi($path,$data,$method);
		return json_decode($dataresult)->data->rows;
	}

	function getDistricts($provinceId,$cityId) {
		$path = "location/province/".$provinceId."/city/" . $cityId . "/district";
		$data = array();
		$method = "GET";
		$dataresult = $this->callApi($path,$data,$method);
		return json_decode($dataresult)->data->rows;
	}

	// User Function
	function getUserLogged(){
		// echo "<pre>";print_r($_COOKIE);echo "</pre>";
		$path = "user/logged";
		$header = array(
			'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$data = array();
		$method = "POST";
		$dataresult = $this->callApi($path,$data,$method,$header);
		return json_decode($dataresult)->data->rows;
	}
	function getUser($userid){
		$path = "user/".$userid;
		$header = array(
			'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$data = array();
		$method = "GET";
		$dataresult = $this->callApi($path,$data,$method,$header);
		return json_decode($dataresult)->data->rows;
	}
	function indexUser($data){
		$data['limit'] = 0;
		$path = "user/" ;
		$header = array(
			'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$method = "GET";
		$dataresult = $this->callApi($path,$data,$method,$header);
		return json_decode($dataresult)->data->rows;
	}
	function getAllUser($rtid){
		$path = "user/" ;
		$header = array(
			'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$data = array(
			'limit' => 0,
			'nid' => $rtid
		);
		$method = "GET";
		$dataresult = $this->callApi($path,$data,$method,$header);
		return json_decode($dataresult)->data->rows;
	}

	function resetPassword($username){
		$path = "user/reset";
		$data = array(
			'username' => $username
		);
		$method = "POST";
		$dataresult = $this->callApi($path,$data,$method);
		return json_decode($dataresult);
	}

	function insertPeople($peoples){
		$path = "user";
		$data = $peoples;
		$method = "POST";
		$dataresult = $this->callApi($path,$data,$method);
		$result = json_decode($dataresult);
		return $result;
	}

	function familyType(){
		$path = "familytype";
		$data = array();
		$header = array(
			'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$method = "GET";
		$dataresult = $this->callApi($path,$data,$method,$header);
		$result = json_decode($dataresult);
		return $result;
	}

	function updateUser($peopleid,$datapost){
		$path = "user/".$peopleid;
		$data = $datapost;
		$header = array(
			'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$method = "PUT";
		$dataresult = $this->callApi($path,$data,$method,$header);
		// print_r($dataresult);exit;
		$result = json_decode($dataresult);
		if( count($result) > 0 ){
			return $result;
		}
		return false;
	}

	function deleteUser($peopleid){
		$path = "user/".$peopleid;
		$data = array();
		$header = array(
			'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$method = "DELETE";
		$dataresult = $this->callApi($path,$data,$method,$header);
		$result = json_decode($dataresult);
		return $result;
	}

	function activateUser($peopleid){
		$path = "user/activate/".$peopleid;
		$data = array();
		$header = array(
			'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$method = "POST";
		$dataresult = $this->callApi($path,$data,$method,$header);
		$result = json_decode($dataresult);
		return $result;
	}

	function getFamily($familyId, $pid = null){
		$path = "user/";
		$data = array(
			"family_id" => $familyId,
			"limit" => 0,
		);
		if( $familyId == "" || is_null( $familyId ) ){
			$path = "user/" . $pid;
			// $data = array(
			// 	"pid" => $pid,
			// 	"limit" => 0,
			// );
		}
		$header = array(
			'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$method = "GET";
		$dataresult = $this->callApi($path,$data,$method,$header);
		$result = json_decode($dataresult);
		return $result;
	}

	function inviteUser($datauser){
		$path = "user/invite";
		$data = $datauser;
		$header = array(
			'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$method = "POST";
		$dataresult = $this->callApi($path,$data,$method,$header);
		$result = json_decode($dataresult);
		return $result;
	}

	function insertFamily($datauser,$familyId){
		$path = "family/".$familyId;
		$data = $datauser;
		$header = array(
			'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$method = "POST";
		$dataresult = $this->callApi($path,$data,$method,$header);
		$result = json_decode($dataresult);
		return $result;
	}

	// Invitor Function
	function insertInvitor($datapost){
		$path = "user/invitor";
		$data = $datapost;
		$method = "POST";
		$dataresult = $this->callApi($path,$data,$method);
		$result = json_decode($dataresult);
		return $result;
	}


	// Login Function
	function login($logindata){
		$path = "user/login";
		$data = $logindata;
		$method = "POST";
		$dataresult = $this->callApi($path,$data,$method);
		$result = json_decode($dataresult);
		return $result;
	}

  // function updateUser($userId, $data){
  //   $path = "user/". $userId;
  //   $method = "PUT";
  //   $header = array(
  //     'Authorization: Bearer ' . $_COOKIE['accesstoken']
  //   );
  //   $dataresult = $this->callApi($path, $data, $method, $header);
  //   $result = json_decode($dataresult);
  //   return $result;
  // }


	// RT Functions
	function insertRT($datart){
		$path = "rt";
		$data = $datart;
		$method = "POST";
		$dataresult = $this->callApi($path,$data,$method);
		return json_decode($dataresult);
	}

	function getRT( $rtid ){
		$path = "rt/".$rtid;
		$data = array();
		$method = "GET";
		$header = array(
			'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$dataresult = $this->callApi($path,$data,$method,$header);
		$result = json_decode($dataresult);
		return $result;
	}

	function updateRT($datapost,$rtid){
		$path = "rt/".$rtid;
		$data = $datapost;
		$method = "PUT";
		$header = array(
			'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$dataresult = $this->callApi($path,$data,$method,$header);
		$result = json_decode($dataresult);
		return $result;
	}


	//finance API
  function createFinance( $data ){
    $path = "finance";
    $method = "POST";
    $header = array(
    'Authorization: Bearer ' . $_COOKIE['accesstoken']
    );
    $dataresult = $this->callApi($path,$data,$method,$header);
    $result = json_decode($dataresult);
    return $result;
  }

  function getFinance( $id ){
		$path = "finance/".$id;
		$data = array('limit' => 0);
		$method = "GET";
		$header = array(
				'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$dataresult = $this->callApi($path,$data,$method,$header);
		$result = json_decode($dataresult);
		return $result;
	}

	function indexFinance($params = array()){
		$path = "finance";
		$data = $params;
		$data['limit'] = 0;
		$method = "GET";
		$header = array(
				'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$dataresult = $this->callApi($path,$data,$method,$header);
		$result = json_decode($dataresult);
		return $result;
	}

	//event API
	function getEvent( $id ){
		$path = "event/".$id;
		$data = array('limit' => 0);
		$method = "GET";
		$header = array(
				'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$dataresult = $this->callApi($path,$data,$method,$header);
		$result = json_decode($dataresult);
		return $result;
	}

	function indexEvent($params = array()){
		$path = "event";
		$data = $params;
		$data['limit'] = 0;
		$method = "GET";
		$header = array(
				'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$dataresult = $this->callApi($path,$data,$method,$header);
		$result = json_decode($dataresult);
		return $result;
	}

	function createEvent( $data ){
		$path = "event";
		$method = "POST";
		$header = array(
				'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$dataresult = $this->callApi($path,$data,$method,$header);
		$result = json_decode($dataresult);
		return $result;
	}

	function updateEvent( $id, $data ){
		$path = "event/". $id;
		$method = "PUT";
		$header = array(
				'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$dataresult = $this->callApi($path,$data,$method,$header);
		$result = json_decode($dataresult);
		return $result;
	}

	function deleteEvent($id){
		$path = "event/". $id;
		$method = "DELETE";
		$header = array(
				'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$data = array();
		$dataresult = $this->callApi($path, $data, $method, $header);
		$result = json_decode($dataresult);
		return $result;
	}

	// Alarm API
	function insertAlarm($data){
		$path = "alert";
		$method = "POST";
		$header = array(
			'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$data = $data;
		$dataresult = $this->callApi($path, $data, $method, $header);
		$result = json_decode($dataresult);
		return $result;
	}

  function getAlarm($neighbourhood){
    $path = "alert";
    $method = "GET";
    $header = array(
    'Authorization: Bearer ' . $_COOKIE['accesstoken']
    );
    $data = array(
    "neighbourhood_id" => $neighbourhood,
    "limit" => 0
    );
    $dataresult = $this->callApi($path, $data, $method, $header);
		$result = json_decode($dataresult);
		return $result;

	}
    function getAlarmDetail($alarmid){
      $path = "alert/".$alarmid;
      $method = "GET";
      $header = array(
      	'Authorization: Bearer ' . $_COOKIE['accesstoken']
      );
      $data = array();
      $dataresult = $this->callApi($path, $data, $method, $header);
      $result = json_decode($dataresult);
      return $result;
    }

//report api
  function getReport( $id )
  {
    $path = "report/" . $id;
    $data = array('limit' => 0);
    $method = "GET";
    $header = array(
    'Authorization: Bearer ' . $_COOKIE['accesstoken']
    );
    $dataresult = $this->callApi($path, $data, $method, $header);
    $result = json_decode($dataresult);
  	return $result;
  }

	function indexReport($params = array()){
		$path = "report";
		$data = $params;
		$data['limit'] = 0;
		$method = "GET";
		$header = array(
				'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$dataresult = $this->callApi($path,$data,$method,$header);
		$result = json_decode($dataresult);
		return $result;
	}

	function createReport( $data ){
		$path = "report";
		$method = "POST";
		$header = array(
				'Authorization: Bearer ' . $_COOKIE['accesstoken']
		);
		$dataresult = $this->callApi($path,$data,$method,$header);
		$result = json_decode($dataresult);
		return $result;
	}

	function updateReport( $id, $data )
  {
    $path = "report/" . $id;
    $method = "PUT";
    $header = array(
    'Authorization: Bearer ' . $_COOKIE['accesstoken']
    );
    $dataresult = $this->callApi($path, $data, $method, $header);
    $result = json_decode($dataresult);
    return $result;
  }

  function deleteReport($id){
    $path = "report/". $id;
    $method = "DELETE";
    $header = array(
      'Authorization: Bearer ' . $_COOKIE['accesstoken']
    );
    $data = array();
    $dataresult = $this->callApi($path, $data, $method, $header);
    $result = json_decode($dataresult);
    return $result;
  }

  	function appBuildVersion($usertoken = NULL) {
		$path = 'application/build';
		$method = 'GET';
		/*$header = array(
	      'Authorization: Bearer ' . $usertoken
	    );*/
		$data = array();
		$call = $this->callApi($path, $data, $method,[]);
		$result = json_decode($call);
		return $result;
	}

	public function getActivity($page)
	{
		$path = 'activity';
		$header = array(
      'Authorization: Bearer ' . $_COOKIE['accesstoken']
    );
		$params = [
			'page' => $page
		];
		$resultRaw = $this->callApi($path, $params, 'GET', $header);
		$result = json_decode($resultRaw, true);

		return $result;
	}

	public function getSingleActivity($id)
	{
		$path = 'activity/' . $id;
		$header = array(
      'Authorization: Bearer ' . $_COOKIE['accesstoken']
    );
		$resultRaw = $this->callApi($path, [], 'GET', $header);

		$result = json_decode($resultRaw, true);

		return $result;
	}

	public function getComments($activityId)
	{
		$path = 'activity/' . $activityId . '/comments';
		$header = array(
      'Authorization: Bearer ' . $_COOKIE['accesstoken']
    );
		$resultRaw = $this->callApi($path, [], 'GET', $header);

		$result = json_decode($resultRaw, true);

		return $result;
	}

	public function postComment($activityId, $content)
	{
		$path = 'activity/' . $activityId . '/comments';
		$params = [
			'content' => $content,
		];
		$header = array(
      'Authorization: Bearer ' . $_COOKIE['accesstoken']
    );
		$resultRaw = $this->callApi($path, $params, 'POST', $header);

		return json_decode($resultRaw, true);
	}

	public function postStatus($content, $title = null)
	{
		$path = 'activity';
		$params = [
			'title' => $title,
			'content' => $content,
		];
		$header = array(
      'Authorization: Bearer ' . $_COOKIE['accesstoken']
    );
		$resultRaw = $this->callApi($path, $params, 'POST', $header);

		return json_decode($resultRaw, true);
	}

	//Helper function untuk waktu
	// Bikin biar bisa seperti 10 menit yang lalu dll
	// Thanks internet
	// http://stackoverflow.com/questions/1416697/converting-timestamp-to-time-ago-in-php-e-g-1-day-ago-2-days-ago
	public function time_elapsed_string($ptime)
	{
	    $etime = time() - $ptime;

	    if ($etime < 1)
	    {
	        return '0 detik';
	    }

	    $a = array( 365 * 24 * 60 * 60  =>  'tahun',
	                 30 * 24 * 60 * 60  =>  'bulan',
	                      24 * 60 * 60  =>  'hari',
	                           60 * 60  =>  'jam',
	                                60  =>  'menit',
	                                 1  =>  'detik'
	                );

	    foreach ($a as $secs => $str)
	    {
	        $d = $etime / $secs;
	        if ($d >= 1)
	        {
	            $r = round($d);
	            return $r . ' ' . $str . ' yang lalu';
	        }
	    }
	}

	// return string
	function timeAgo($datetime) {
		$datetime = \DateTime::createFromFormat('Y-m-d H:i', $datetime);

		return $this->time_elapsed_string($datetime->getTimestamp());
	}

	function peopleExport(){
		$path = "user/excel";
	    $method = "GET";
	    $header = array(
	      'Authorization: Bearer ' . $_COOKIE['accesstoken']
	    );
	    $data = array();
	    $dataresult = $this->callApi($path, $data, $method, $header);
	    $result = json_decode($dataresult);
	    return $result;
	}


	// Main Function to call API
	function callApi($path, $data, $method,$header=array()) {
		foreach($data as $dt => $val){
			$data[$dt] = htmlspecialchars($val);
		}
		global $apiurl;
		$data = http_build_query($data, '', '&');
		$parse = parse_url($apiurl . $path);

		$path = $parse['scheme']. '://'. $parse['host']. $parse['path'];
		if (isset($parse['query'])) {
			$path .= '?'. $parse['query'];
		}

		if ($method == 'POST' || $method == 'PUT' || $method == 'DELETE') {
			$Curl_Session = curl_init($path);
			curl_setopt($Curl_Session, CURLOPT_POST, 1);
			curl_setopt($Curl_Session, CURLOPT_POSTFIELDS, $data);
		} else if ($method == 'GET') {
			$Curl_Session = curl_init($path . '?' . $data);
		}

		curl_setopt($Curl_Session, CURLOPT_HEADER, FALSE);
		if (count($header)) {
			curl_setopt($Curl_Session, CURLOPT_HTTPHEADER, $header);
		}
		curl_setopt($Curl_Session, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($Curl_Session, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($Curl_Session, CURLOPT_ENCODING, 1);
		curl_setopt($Curl_Session, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($Curl_Session, CURLOPT_CONNECTTIMEOUT, 2);
		curl_setopt($Curl_Session, CURLOPT_TIMEOUT, 10);
		if (isset($parse['port'])) {
			curl_setopt($Curl_Session, CURLOPT_PORT, $parse['port']);
		}

		$output = curl_exec($Curl_Session);
		if ($curl_error_number = curl_errno($Curl_Session)) {
			return $output;
		}
		else {
			$header = curl_getinfo($Curl_Session);
		}
		curl_close($Curl_Session);
		return $output;
	}



}
$functions = new Functions();

// Get Master data
if(isset($_GET['type'])){
	if($_GET['type']=="getcity"){
		$provinceid = $_POST['provinceid'];
		$cities = $functions->getCities($provinceid);
		$html = "<option value=''>Pilih Kabupaten / Kota</option>";
		foreach($cities as $value){
			$html .= "<option value='".$value->cid."'>".$value->name."</option>";
		}
		echo $html;
	}
	if($_GET['type']=="getdistrict"){
		$cityid = $_POST['cityid'];
		$provinceid = $_POST['provinceid'];
		$districts = $functions->getDistricts($provinceid,$cityid);
		$html = "<option value=''>Pilih Kecamatan</option>";
		foreach($districts as $value){
			$html .= "<option value='".$value->did."'>".$value->name."</option>";
		}
		echo $html;
	}
}

if( !function_exists("accounting_format") ){
	function accounting_format($number, $decimal_place){
		if( $number > 0 ){
			return number_format($number, $decimal_place, ',', '.');
		}
		else {
			return number_format($number, $decimal_place, ',', '.');
		}
	}
}

if( !function_exists("redirect") ){
	function redirect($url){
		foreach ($_SERVER as $key => $value) {
			if( strpos($key, "HTTP_") !== FALSE ){
				header($key . ": " . $value);
			}
		}
		header("location:" . $url);
	}
}
