<?php 
	setcookie("demoregister",true,time()+3600*24*365);
	$_COOKIE['demoregister'] = true;
	$result = array( "step" => "","status" => false );
	require("system/functions.php");

	function insert_rt() {
		$functions = new Functions();
		$neighbourhood=array(
			'rt' => '01',
			'rw' => '01',
			'village' => 'Demo Kentongan',
			'district_id' => "3638",
			'city_id' => "400",
			'province_id' => "26",
			'country' => "Indonesia",
		);
		$insert_rt = $functions->insertRT($neighbourhood);
		return $insert_rt;		
	}

	function insert_dummy_data(){
		$functions = new Functions(); 
		// Insert Kas
		$finance[] = array(
			'type' => 'I',
			'description' => 'Saldo awal kas',
			'amount' => '400000',
			'neighbourhood_id' => $_COOKIE['rtid'],
			'tag' => "Kas Awal"
		);
		$finance[] = array(
			'type' => 'I',
			'description' => 'Iuran warga',
			'amount' => '250000',
			'neighbourhood_id' => $_COOKIE['rtid'],
			'tag' => "Iuran"
		);
		$finance[] = array(
			'type' => 'O',
			'description' => 'Kerja Bakti',
			'amount' => '150000',
			'neighbourhood_id' => $_COOKIE['rtid'],
			'tag' => "Belanja"
		);
		$finance[] = array(
			'type' => 'O',
			'description' => 'Reparasi Lampu Jalan',
			'amount' => '55000',
			'neighbourhood_id' => $_COOKIE['rtid'],
			'tag' => "Belanja"
		);
		// foreach($finance as $data){
		// 	$insert_cash = $functions->createFinance($data);
		// }

		// Insert Alert
		$alert[] = array(
			'sender_pid' => $_COOKIE['peopleid'] ,
			'type' => "1",
			'latitude' => "-7.776525",
			'longitude' => "110.346409",
		);
		$alert[] = array(
			'sender_pid' => $_COOKIE['peopleid'] ,
			'type' => "2",
			'latitude' => "-7.776530",
			'longitude' => "110.346420",
		);
		$alert[] = array(
			'sender_pid' => $_COOKIE['peopleid'] ,
			'type' => "2",
			'latitude' => "-7.776510",
			'longitude' => "110.346400",
		);
		// foreach($alert as $data){
		// 	$insert_alert = $functions->insertAlarm($data);
		// }

		// Insert Kegiatan
		$event = array(
			'name' => 'Kumpulan RT',
			'start_date' => date("Y-") . date("m-") . date("d ") . "20:00",
			'end_date' => date("Y-") . date("m-") . date("d ") . "22:00",
			'location' => 'Rumah Ketua RT',
			'neighbourhood_id' => $_COOKIE['rtid'],
			'type' => "Kumpulan RT",
			'description' => "Harap datang tepat waktu karena pentingnya acara"
		);
		// $insert_event = $functions->createEvent($event);

		$report[] = array(
			'sender_pid' => $_COOKIE['peopleid'] ,
			'description' => "Lampu jalan kemarin mati dan belum dibenerin",
			'response' => "",
			'neighbourhood_id' => $_COOKIE['rtid'],
		);
		$report[] = array(
			'sender_pid' => $_COOKIE['peopleid'] ,
			'description' => "Jl. Mawar tergenang air, got perlu dibersihkan di kerja bakti selanjutnya",
			'response' => "",
			'neighbourhood_id' => $_COOKIE['rtid'],
		);
		// foreach($report as $data){
		// 	$insert_report = $functions->createReport($data);
		// }

		// Inserting data
		$insert_cash = $functions->createFinance($finance[0]);
		sleep(0.5);
		$insert_report = $functions->createReport($report[0]);
		sleep(0.5);
		$insert_alert = $functions->insertAlarm($alert[0]);
		sleep(0.5);
		$insert_event = $functions->createEvent($event);
		sleep(0.5);
		$insert_cash = $functions->createFinance($finance[1]);
		sleep(0.5);
		$insert_alert = $functions->insertAlarm($alert[1]);
		sleep(0.5);
		$insert_report = $functions->createReport($report[1]);
		sleep(0.5);
		$insert_cash = $functions->createFinance($finance[2]);
		sleep(0.5);
		$insert_alert = $functions->insertAlarm($alert[2]);
		sleep(0.5);
		$insert_cash = $functions->createFinance($finance[3]);
	}

	function insert_user($insert_rt) {
		$functions = new Functions(); 
		$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
		$email = '';
		$random_string_length = 10;
		for ($i = 0; $i < $random_string_length; $i++) {
		  $email .= $characters[rand(0, strlen($characters) - 1)];
		}
		$nid = $insert_rt->data->rows[0]->nid;
		$peoples = array(
			'name' => "Ketua RT Demo",
			'card_id' => "00998877",
			'family_id' => "99008877",
			//'email' => "email" . substr(uniqid(mt_rand(),true),0,7) . mt_rand(1, 10)."@kentongan.org",
			'email' => "email_" . $email."@kentongan.org",
			'phone' => "+620000000" . rand(1000,9999),
			'role' => 'rt',
			'neighbourhood_id' => $nid,
		);
		$insert_people = $functions->insertPeople($peoples);
		return $insert_people;
	}

	function test_rt() {
		$insert_rt = insert_rt();
		if(@$insert_rt->code == 200){
			$insert_people = insert_user($insert_rt);
			if( @$insert_people->status){
				$data = $insert_people->data->rows;
				$data=$data[0];
				$token = $data->token;
				$result['status'] = true;

				setcookie("peopleid",$data->pid,time()+3600*24*365);
				setcookie("rtid",$data->neighbourhood->nid,time()+3600*24*365);
				setcookie("kentongantype","rt",time()+3600*24*365);
				setcookie("accesstoken",$token,time()+3600*24*365);

				$_COOKIE['peopleid'] = $data->pid;
				$_COOKIE['rtid'] = $data->neighbourhood->nid;
				$_COOKIE['kentongantype'] = "rt";
				$_COOKIE['accesstoken'] = $token;
				?>
				<script type='text/javascript'>
					var exdays = 365;
					var d = new Date();
				 	d.setTime(d.getTime() + (exdays*24*60*60*1000));
				 	var expires = "expires="+d.toUTCString();

					document.cookie="peopleid=<?php echo $data->pid; ?>;" + expires;
					document.cookie="rtid=<?php echo $data->neighbourhood->nid; ?>; " + expires;
					document.cookie="kentongantype=rt;" + expires;
					document.cookie="accesstoken=<?php echo $token; ?>;" + expires;
				</script>
				<?php
				return $result;
			} else {
				test_rt();
				return;
			}
		} else {
			test_rt();
			return;
		}
	}
	$result = test_rt();
	
 	if( $result['status'] != true){
		redirect("login.php?message=test_rt_error");	
	}
	else {
		insert_dummy_data();
		redirect("blocker.php");
	}
?>